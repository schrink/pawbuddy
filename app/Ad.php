<?php

namespace App;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Notifications\Notifiable;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
//use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use BrianFaust\Reviewable\HasReviews;
/**
 * Class Ad
 * @package App\Models
 * @version January 29, 2017, 6:18 pm UTC
 */
class Ad extends Model implements HasMediaConversions
{
    use Sluggable, HasMediaTrait, HasReviews, Notifiable;

    public $table = 'ads';
    protected $with = ['user', 'reviews', 'media', 'services'];

    protected $appends = ['excerpt', 'avatar', 'distance', 'servicesFull', 'rating'];

    protected $hidden = ['paypal_verification_token'];

    public $fillable = [
        'user_id',
        'published',
        'title',
        'about',
        'residence',
        'kids',
        'car',
        'other_pets',
        'experience',
        'cynologist',
        'veterinary',
        'sizes',
        'ages',
        'breeds_to_care'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [

        'published' => 'boolean',
        'title' => 'string',
        'about' => 'text',
        'residence' => 'enum',
        'kids' => 'enum',
        'car' => 'enum',
        'days' => 'array',
        'experience' => 'string',
        'cynologist' => 'enum',
        'veterinary' => 'enum',
        'times' => 'array',
        'ages' => 'array',
        'breeds_to_care' => 'array'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required',
        'description' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\User::class, 'user_id', 'id');
    }

    public function userId()
    {
        return $this->belongsTo(\App\User::class, 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function services()
    {
        return $this->belongsToMany(\App\Service::class)
            ->withPivot([
                'id',
                'price',
                'holiday_price',
                'additional_dog_price',
                'pick_up_drop_off',
                'more_days_discount',
                'max_no_of_dogs',
                'days',
                'times']);
    }

    /*
    * Relatinships
    */

    public function status()
    {
        return $this->belongsTo(\App\AdStatus::class);
    }

    public function sizes()
    {
        return $this->belongsToMany(\App\Size::class);
    }

    public function other_pets()
    {
        return $this->belongsToMany(\App\AnimalType::class, 'ad_animal_type', 'ad_id', 'animal_type_id');
    }

    public function breeds_to_care()
    {
        return $this->belongsToMany(\App\Breed::class, 'ad_breed', 'ad_id', 'breed_id');
    }

    public function accepting_ages(){
        return $this->belongsToMany(\App\Age::class, 'ad_age', 'ad_id', 'age_id');
    }

    public function allowances()
    {
        return $this->belongsToMany(\App\Allowance::class, 'ad_allowance', 'ad_id', 'allowance_id');
    }


    public function home_features()
    {
        return $this->belongsToMany(\App\HomeFeature::class, 'ad_home_feature', 'ad_id', 'home_feature_id');
    }


    public function badges()
    {
        return $this->belongsToMany(\App\Badge::class, 'ad_badge', 'ad_id', 'badge_id');
    }

    public function busy_days()
    {
        return $this->belongsToMany(\App\BusyDay::class, 'ad_busy_days', 'ad_id', 'busy_day_id');
    }

    public function bookings()
    {
        return $this->hasMany(\App\Order::class);
    }

	public function tags()
	{
		return $this->morphToMany('App\Tag', 'taggable');
	}

    /*
     * Custom attributes
     */


    public function getExcerptAttribute($value)
    {
        return str_limit(strip_tags($this->about), 100, '...');
    }

    public function getAvatarAttribute()
    {
        return \Helpers::getProfileImage($this->user);
    }

    public function getDistanceAttribute()
    {
        return \Helpers::getDistance($this->user->lat_lng['lat'], $this->user->lat_lng['lng'], \Auth::user()->lat_lng['lat'], \Auth::user()->lat_lng['lng']);
    }

    public function getServicesFullAttribute()
    {
        foreach ($this->services as $key => $service)
        {
            $this->services[$key]['price'] = number_format($service->pivot->price, 2, ",", '.');
        }

        return $this->services;

    }
    public function getRatingAttribute()
    {
        return $this->getRating();
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => ['user.name', 'title'],
                'onUpdate' => true
            ]
        ];
    }

    public function registerMediaConversions()
    {
        $this->addMediaConversion('thumb')
            ->width(340)
            ->sharpen(10)
            ->performOnCollections('images');
	    $this->addMediaConversion('carousel')
		    ->fit('crop', 730, 400)
	         ->sharpen(10)
	         ->performOnCollections('images');
    }
    public function getRating(): float
    {
        return round($this->reviews->avg('rating'), 1);
    }

    public function routeNotificationForMail()
    {
        return $this->paypal_email;
    }


}
