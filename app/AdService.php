<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdService extends Model
{
    protected $casts = [
      'days' => 'array',
      'times' => 'array',
    ];

    public $with=['preferences', 'service'];
    protected $table="ad_service";

    public function service()
    {
        return $this->belongsTo(\App\Service::class);
    }
    public function preferences()
    {
        return $this->belongsToMany(\App\Preference::class, 'ad_service_preference', 'ad_service_id', 'preference_id');
    }

	public function tags()
	{
		return $this->morphToMany('App\Tag', 'taggable');
	}
}
