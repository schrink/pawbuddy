<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdStatus extends Model
{
    protected $fillable = ['title'];
    public $timestamps = false;

    public function ads()
    {
        return $this->hasMany(\App\Ad::class);
    }
}
