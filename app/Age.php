<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Age extends Model
{
    protected $fillable = ['title', 'months'];
    protected $cast = ['title' => 'string'];
    public $timestamps = false;
}
