<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Allowance extends Model
{
    public $timestamps = false;
    protected $fillable = ['title', 'icon'];
}
