<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnimalType extends Model
{
    public $fillable = ['type', 'lang'];
    public $timestamps = false;

    public function breeds(){
        return $this->hasMany('App\Breed');
    }
}
