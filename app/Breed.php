<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Breed extends Model
{
    public $timestamps =  false;
    public $fillable = ['breedname', 'animal_type_id', 'lang'];

    public function pets(){
        return $this->hasMany('App\Pet');
    }

    public function animal_type(){
        return $this->belongsTo('App\AnimalType');
    }
}
