<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusyDay extends Model
{
    public $timestamps = false;
}
