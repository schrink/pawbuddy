<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeFeature extends Model
{
    protected $fillable = ['title', 'icon'];
    public $timestamps = false;
}
