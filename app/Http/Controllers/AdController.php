<?php

namespace App\Http\Controllers;

use App\AdService;
use App\Age;
use App\Allowance;
use App\AnimalType;
use App\Breed;
use App\Ad;
use App\Events\SitterRegistered;
use App\HomeFeature;
use App\Preference;
use App\Service;
use App\Repositories\AdRepository;
use App\Size;
use Illuminate\Http\Request;
use Flash;
use Response;
use Validator;
use Alert;
use App\Notifications\SendPaypalNotificationCode;

class AdController extends AppBaseController {
    /** @var  AdRepository */
    private $adRepository;

    public function __construct(AdRepository $adRepo)
    {
        $this->adRepository = $adRepo;
    }

    /*
     *  List Sitters
     */

    public function index()
    {
        $minMax = $this->getMinMaxPrices();

        $ads = Ad::where('published', 1)
            ->with(['reviews', 'user' => function ($u)
            {
                $u->with(['pets']);
            }, 'services'])
            ->whereHas('services', function ($q)
            {
                $q->where('service_id', 1);
            })
            ->withCount(['bookings'])
            ->limit(20)
            ->get();

        $sizes = Size::all();

        return view('front.ads', compact('ads', 'sizes', 'minMax'))->with(['folder' => 'sitters']);
    }

    /*
     * Display forms for creating & updating sitters
     */
    public function create($step = null)
    {
        $ad = Ad::firstOrCreate(['user_id' => \Auth::user()->id], ['slug' => 'new', 'description' => '']);
        $step = ( ! isset($step)) ? $ad->step_finished + 1 : $step;
        switch ($step)
        {
            case 1:
                $other_pets = $this->prepareSelectValues($ad->other_pets->pluck('id'));
                list($cynologyCertificates, $veterinaryCertificates, $user, $at) = $this->registerFormStep1($ad);

                return view('front.ad-form', compact('ad', 'cynologyCertificates', 'veterinaryCertificates', 'user', 'step', 'at', 'other_pets'))->with(['folder' => 'sitters-register']);
                break;
            case 2:
                $services = Service::all();
                $selectedServices = [];
                $selectedDays = [];
                $selectedTimes = [];

                foreach ($services as $s)
                {
                    $selectedServices[$s->slug] = ( ! $ad->services->where('slug', $s->slug)->isEmpty()) ? true : false;
                    $selectedDays[$s->slug] = ( ! $ad->services->where('slug', $s->slug)->isEmpty()) ? json_decode($ad->services->where('slug', $s->slug)->first()->pivot->days) : [];
                    $selectedTimes[$s->slug] = ( ! $ad->services->where('slug', $s->slug)->isEmpty()) ? json_decode($ad->services->where('slug', $s->slug)->first()->pivot->times) : [];
                }

                return view('front.ad-form', compact('ad', 'step', 'user', 'services', 'selectedServices', 'selectedDays', 'selectedTimes'))->with(['folder' => 'sitters-register']);
                break;
            case 3:
                $services = Service::all();
                $minMax = $this->getMinMaxPrices();

                return view('front.ad-form', compact('ad', 'step', 'user', 'services', 'minMax'))->with(['folder' => 'sitters-register']);
                break;
            case 4:
                $allowances = Allowance::all();
                $features = HomeFeature::all();
                $images = $ad->getMedia('images');
                $ad->with('allowances', 'home_features');
                $selectedFeatures = [];
                foreach ($ad->home_features as $feature)
                {
                    $selectedFeatures[$feature->id] = true;
                }

                $selectedAllowances = [];
                foreach ($ad->allowances as $feature)
                {
                    $selectedAllowances[$feature->id] = true;
                }

                return view('front.ad-form', compact('ad', 'step', 'user', 'allowances', 'features', 'images', 'selectedFeatures', 'selectedAllowances'))->with(['folder' => 'sitters-register']);
                break;

            case 5:
                $services = Service::all();
                $preferences = Preference::all();
                $selectedPreferences = [];

                $adServices = AdService::where('ad_id', $ad->id)->get();
                foreach ($adServices as $services)
                {
                    foreach ($services->preferences as $preference)
                    {
                        $selectedPreferences[$preference->pivot->ad_service_id][$preference->pivot->preference_id] = true;
                    }
                }

                $sizes = Size::all();
                $ages = Age::all();

                return view('front.ad-form', compact('ad', 'step', 'user', 'services', 'preferences', 'selectedPreferences', 'sizes', 'ages'))->with(['folder' => 'sitters-register']);
                break;

            case 6:
                return view('front.ad-form', compact('ad', 'step', 'user'))->with(['folder' => 'sitters-register']);
                break;

            case 7:

                return redirect(route('my-profile'));
            break;
        }

    }

    /*
     * Store sitter settings
     */
    public function store(Request $request)
    {

        /*
        * STEP 1
        */
        switch ($request->step_finished)
        {
            case 1:
                $request->merge(['user_id' => \Auth::user()->id]);
                $ad = Ad::updateOrCreate(['user_id' => \Auth::user()->id], $request->except(['_token', 'other_pets', 'step_finished']));
                $ad->other_pets()->sync($request->other_pets);
                $user = \Auth::user();
                $user->role_id = 5;
                $user->save();
                break;

            case 2:

                $ad = Ad::find($request->ad);

                foreach (json_decode($request->services) as $key => $service)
                {
                    if ($service)
                    {
                        $service = Service::where('slug', $key)->first();
                        $servicesAr[$service->id]['days'] = json_encode(json_decode($request->days)->{$key});
                        $servicesAr[$service->id]['times'] = json_encode(json_decode($request->times)->{$key});
                    }
                }
                if ( ! isset($servicesAr))
                {
                    Alert::error('Morate odabrati makar jednu uslugu!')->autoclose(2500);

                    return back();
                }

                $ad->services()->sync($servicesAr);
                break;

            case 3 :
                $ad = Ad::find($request->ad);
                foreach ($request->prices as $id => $prices)
                {
                    if (isset($prices['pick_up_drop_off']))
                    {
                        AdService::where('id', $id)->update(['price' => $prices['price'], 'holiday_price' => $prices['holiday_price'], 'additional_dog_price' => $prices['additional_dog_discount'], 'more_days_discount' => $prices['more_days_discount'], 'pick_up_drop_off' => $prices['pick_up_drop_off']]);
                    } else
                    {
                        AdService::where('id', $id)->update(['price' => $prices['price'], 'holiday_price' => $prices['holiday_price'], 'additional_dog_price' => $prices['additional_dog_discount'], 'more_days_discount' => $prices['more_days_discount']]);
                    }
                }

                break;
            case 4 :
                $ad = Ad::find($request->ad);
                $ad->about_home = $request->about_home;
                $ad->save();
                break;

            case 5:
                $ad = Ad::find($request->ad);
                foreach ($request->services as $key => $value)
                {
                    AdService::where('id', $key)->update(['max_no_of_dogs' => $value['maximum_no_of_dogs']]);
                }
                break;
            case 6:
                $ad = Ad::find($request->ad);
                $ad->paypal_name = $request->paypal_name;
                $ad->paypal_lastname = $request->paypal_lastname;
                if($ad->paypal_email != $request->paypal_email) {
                    $token  = str_random(20);
                    $ad->paypal_verification_token = $token;
                    $ad->paypal_verified = 0;
                    $ad->paypal_email = $request->paypal_email;
                    $ad->save();
                    $ad->notify(new SendPaypalNotificationCode($ad));
                }

                if($request->tos == 1){

                    $ad->tos = "yes";
                    $ad->save();
                } else {
                    Alert::error('Morate da prihvatite Terms of Service');
                    return back();
                }
                Alert::success('Uspešno ste završili registraciju. Uskoro ćemo proveriti Vašu prijavu i obavestiti Vas kada je Vaš profil odobren', 'Čestitamo!')->persistent('Zatvori');
                event(new SitterRegistered($ad));
                break;

        }


        $ad->step_finished =
            ($ad->step_finished > $request->step_finished) ?
                $ad->step_finished : $request->step_finished;
        $ad->save();

        return redirect(route('sitters.register', (int)$request->step_finished + 1));
    }

    /*
     * Show single sitter
     */

    public function show($slug)
    {
        $ad = Ad::where('slug', $slug)->with(['user'])->first();
        $user = $ad->user;
        $adServices = AdService::where('ad_id', $ad->id)->get();

        return view('ads.show', compact('ad', 'user', 'adServices'))->with(['folder' => 'sitters-profile']);
    }




    public function contact($slug)
    {
        $ad = Ad::where('slug', $slug)->with(['user'])->first();

        $user = $ad->user;
        list($dogs, $cats, $other, $animalTypes) = app('App\Http\Controllers\PetController')->preparePets();
        return view('ads.contact', compact('ad', 'user', 'dogs', 'cats', 'other', 'animalTypes'))->with(['folder' => 'sitters-profile']);
    }


    public function updateSwitches(Request $request)
    {

        if ($request->model == "services")
        {
            $ad = AdService::where(['ad_id' => $request->adId, 'service_id' => $request->serviceId])->first();
            if ($request->value == true)
            {
                $ad->{$request->resource}()->attach($request->id);
            } else
            {
                $ad->{$request->resource}()->detach($request->id);
            }

            return $request->all();
        } else
        {
            $ad = Ad::where(['user_id' => \Auth::user()->id])->first();
            if ($request->value == true)
            {
                $ad->{$request->resource}()->attach($request->id);
            } else
            {
                $ad->{$request->resource}()->detach($request->id);
            }

            return $request->all();
        }

    }

    public function updateAppButtons(Request $request)
    {
        $ad = Ad::where(['user_id' => \Auth::user()->id])->first();
        $ad->{$request->resource}()->sync($request->value);
    }

    /*
     * HELPER METHODS
     */

    /**
     * @param $animal_types
     * @return string
     */
    private function prepareSelectInput($animal_types)
    {
        $len = count($animal_types);
        $i = 0;
        $at = "[";
        foreach ($animal_types as $key => $type)
        {
            $at .= "{id:'$key', text: '$type'}";
            if ($i == $len - 1)
            {

            } else
            {
                $at .= ",";
            }
        }
        $at .= "]";

        return $at;
    }

    /**
     * @param $ad
     * @return array
     */
    private function registerFormStep1($ad)
    {
        $cynologyCertificates = $ad->getMedia('cynology_certificates');
        foreach ($cynologyCertificates as $key => $mediaItem)
        {
            $cynologyCertificates[$key]['url'] = $mediaItem->getUrl();
        }
        $veterinaryCertificates = $ad->getMedia('veterinary_certificates');
        foreach ($veterinaryCertificates as $key => $mediaItem)
        {
            $veterinaryCertificates[$key]['url'] = $mediaItem->getUrl();
        }
        $user = \Auth::user();
        $animal_types = AnimalType::all()->pluck('type', 'id');
        $at = $this->prepareSelectInput($animal_types);

        return array($cynologyCertificates, $veterinaryCertificates, $user, $at);
    }

    /**
     * @param $other_pets
     * @return string
     */
    private function prepareSelectValues($other_pets)
    {
        $op = "[";
        foreach ($other_pets as $key => $pet)
        {
            $op .= $pet . ",";
        }
        $other_pets = substr($op, 0, -1) . "]";

        return $other_pets;
    }

    /**
     * @param $minMax
     * @return mixed
     */
    private function getMinMaxPrices()
    {
        for ($i = 1; $i < 4; $i++)
        {
            $minMax[$i]['min'] = AdService::where('service_id', $i)->min('price');
            $minMax[$i]['max'] = AdService::where('service_id', $i)->max('price');
        }

        return $minMax;
    }

    public function checkPayPalEmail($token)
    {
        $ad = Ad::where('paypal_verification_token', $token);
        $ad->paypal_verified = 1;
        $ad->paypal_verification_token = "";
        $ad->save();
    }
}
