<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Ad;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;


class SearchController extends Controller
{


    public function index(Request $request, Ad $ad)
    {
        $filters = json_decode($request->filters);

        if (!isset($filters->service)) {
            $ads = $ad->newQuery();
            $ads->with('services', 'sizes')->where('published', 1);
            return $ads->limit(10)->get();
        }
        $service = $filters->service;
        $ads = $ad->newQuery();
        $ads->with('services', 'sizes')->where('published', 1);

        //search base on service
        if (isset($filters->service))
        {
            $ads->whereHas('services', function ($q) use ($service, $filters)
            {
                $q->where('service_id', $service);
            });
        }

        //search based on price
        if (isset($filters->price))
        {
            $ads->whereHas('services', function ($q) use ($service, $filters)
            {
                $q->where('price', '>', (int)$filters->price[0])
                    ->where('price', '<', (int)$filters->price[1])
                    ->where('service_id', $service);
            });
        }


        //search based on sizes
        if (isset($filters->sizes))
        {
            foreach ($filters->sizes as $sz) {
                $ads->whereHas('sizes', function ($q) use ($service, $filters, $sz)
                {
                    $q->where('ad_size.size_id', $sz);
                });
            }
        }

        //search based on numberOfDogs
        if (isset($filters->noOfDogs))
        {
            $ads->whereHas('services', function ($q) use ($service, $filters)
            {
                $q->where('max_no_of_dogs', '>=', (int)$filters->noOfDogs)
                    ->where('service_id', $service);
            });
        }

        //search based on selected days
        if (isset($filters->selectedDays))
        {
            $ads->whereHas('services', function ($q) use ($service, $filters)
            {
                $days = json_encode($filters->selectedDays);
                $q->whereRaw(DB::raw("JSON_CONTAINS(days, '" . $days ."')"))
                    ->where('service_id', $service);
            });
        }

        //search based on times
        if (isset($filters->selectedTimes))
        {
            $ads->whereHas('services', function ($q) use ($service, $filters)
            {
                $times = json_encode($filters->selectedTimes);
                $q->whereRaw(DB::raw("JSON_CONTAINS(times, '" . $times ."')"))
                    ->where('service_id', $service);
            });
        }

        return $ads->limit(10)->get();
    }
}
