<?php

namespace App\Http\Controllers\Api;

use App\Ad;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SittersController extends Controller
{

    public function show($slug)
    {
        $ad = Ad::where('slug', $slug)->with(['user'])->first();
        $user = $ad->user;

        return $ad;
    }
}
