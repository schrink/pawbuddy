<?php

namespace App\Http\Controllers;

use App\Ad;
use App\Http\Requests\UploadCertificatesRequest;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Spatie\MediaLibrary\Media;

/**
 * @resource File Controller
 *
 * Longer description
 */
class FileController extends Controller
{

    public function upload(Request $request)
    {
        $user = \Auth::user();

        $validator = Validator::make($request->all(), [
            'files' => 'required|mimes:jpeg,bmp,png',
        ]);

        if ($validator->fails()) {
            return;
        }
       // $file = $request->file('files')->store('profile', 'public');

     //   $url = Storage::url($file);
        $user->clearMediaCollection('avatar');
        $user
            ->addMedia($request->file('files'))
            ->toMediaCollection('avatar');
        return  [ 'file' => $user->getFirstMediaUrl('avatar', 'thumb') , 'errcode' => 0 ];
    }

    /**
     * This is the short description [and should be unique as anchor tags link to this in navigation menu]
     *
     * This can be an optional longer description of your API call, used within the documentation.
     *
     */
    public function uploadCertificate(UploadCertificatesRequest $request)
    {

        $ad = Ad::where('user_id', \Auth::user()->id)->first();
        $ad
            ->addMedia($request->file('file'))
            ->withCustomProperties(['title' => $request->headline])
            ->toMediaCollection($request->media);
    }

    public function getCertificatesApi(Request $request)
    {
        $ad = \App\Ad::where('user_id', \Auth::user()->id)->first();
        $mediaItems = $ad->getMedia($request->media);
        foreach ($mediaItems as $key => $mediaItem) {
            $mediaItems[$key]['url'] = $mediaItem->getUrl();
        }
        return $mediaItems;
    }

    public function deleteMediaApi(Request $request)
    {
        $media = Media::find($request->id);
        $media->delete();
        return "Deleted";
    }
    public function downloadCertificateApi(Request $request)
    {
        $media = Media::find($request->id);

        return $media->getUrl();
    }

    public function uploadAdImages(Request $request)
    {

        $ad = \Auth::user()->ad;
        $ad
            ->addMedia($request->file('file'))
            ->toMediaCollection('images');
        return "Success";
    }
}
