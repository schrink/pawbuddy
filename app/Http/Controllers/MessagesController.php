<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;
use Mockery\Exception;

class MessagesController extends Controller
{

	public function send(Request $request) {
		$request->merge(['sender_id' =>  \Auth::user()->id]);

		try {
			Message::create($request->all());
			$messages = Message::where('order_id', $request->order_id)->get();
			return ResponseBuilder::success($messages);
		} catch (Exception $e) {
			return ResponseBuilder::error($e);
		}

    }
}
