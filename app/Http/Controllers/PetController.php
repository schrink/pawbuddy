<?php

namespace App\Http\Controllers;

use App\AnimalType;
use App\Breed;
use App\Http\Requests\CreatePetRequest;
use App\Http\Requests\UpdatePetRequest;
use App\Pet;
use App\Repositories\PetRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use League\Flysystem\Exception;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Alert;
use Validator;

class PetController extends AppBaseController
{
    /** @var  PetRepository */
    private $petRepository;

    public function __construct(PetRepository $petRepo)
    {
        $this->petRepository = $petRepo;
    }


    public function petForm()
    {
        list($dogs, $cats, $other, $animalTypes) = $this->preparePets();

        return view('front.pet-create', compact('dogs', 'cats', 'other', 'animalTypes'))->with(['folder' => 'pet']);
    }

    /**
     * @param Request $request
     */
    public function petSave(Request $request)
    {
		if ($request->image != "undefined") {
	        $validator = Validator::make($request->all(), [
	            'image' => 'mimes:jpeg,bmp,png',
	        ]);

	        if ($validator->fails()) {
	            return abort(406, "File format not acceptable");
	        }
		}
        $request->merge(['user_id' => \Auth::user()->id]);
        //$request->merge(['birthday' => substr($request->birthday, 0, 10)]);

        $request->merge(['birthday' => \Carbon\Carbon::parse($request->birthday)]);
        try{
            $pet = Pet::create($request->except(['imageUrl']));
            if($request->image != "undefined") {
	            $pet
		            ->addMedia( $request->file( 'image' ) )
		            ->toMediaCollection( 'avatar' );
            }
            Alert::success('Dodali ste ljubimca');
        } catch(Exception $e) {
            Alert::error('Greška. Ljubimac nije dodat.');
        }

        return back();
    }

    /**
     * @return array
     */
    public function preparePets()
    {
        $dogs = Breed::where('animal_type_id', '1')->get();
        $d = [];
        foreach ($dogs as $key => $dog)
        {
            $d[$key]['value'] = $dog->id;
            $d[$key]['label'] = $dog->breedname;
        }
        $dogs = json_encode($d);
        $cats = Breed::where('animal_type_id', '2')->get();
        $c = [];
        foreach ($cats as $key => $cat)
        {
            $c[$key]['value'] = $cat->id;
            $c[$key]['label'] = $cat->breedname;
        }
        $cats = json_encode($c);
        $other = Breed::where('animal_type_id', '7')->get();
        $o = [];
        foreach ($other as $key => $oth)
        {
            $o[$key]['value'] = $oth->id;
            $o[$key]['label'] = $oth->breedname;
        }
        $other = json_encode($o);

        $animalTypes = AnimalType::all()->pluck('type', 'id');
        $at = [];
        foreach ($animalTypes as $key => $animalType)
        {
            $at[$key]['value'] = $key;
            $at[$key]['label'] = $animalType;
        }
        $animalTypes = json_encode($at);

        return array($dogs, $cats, $other, $animalTypes);
    }
}
