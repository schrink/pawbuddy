<?php

namespace App\Http\Controllers;

use App\Ad;
use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{

    /*
     * Display my profile
     */
    public function me()
    {
        $ad = Ad::where(['user_id' => \Auth::user()->id])->with(['user', 'bookings'])->first();
        return view('front.me', compact('ad', 'user'))->with(['folder' => 'me']);
    }


    public function profileForm()
    {
        $user = \Auth::user();
        return view('profile', compact('user'))->with(['folder' => 'profile']);
    }

    public function profileUpdate(Request $request)
    {

        //$request->merge(['birthdate' => substr($request->birthdate, 0, 10)]);
        $gm_response = \GoogleMaps::load('geocoding')
            ->setParam (['address' =>$request->address. " " . $request->city . ", " . $request->country])
            ->get();
        $lat = json_decode($gm_response)->results[0]->geometry->location->lat;
        $lng = json_decode($gm_response)->results[0]->geometry->location->lng;
        //$request->merge(['lat_lng' => ['lat'=> $lat, 'lng' => $lng]]);
        $request->merge(['birthdate' => date("Y-m-d", strtotime($request->birthdate))]);
        User::where('id', \Auth::user()->id)
            ->update($request->except('_token', 'personal_id'));
        $user = \Auth::user();
        $user->lat_lng = ['lat'=> $lat, 'lng' => $lng];
        $user->save();
        return back();

    }

    public function whoAmI()
    {
        return \Auth::user();
    }
}
