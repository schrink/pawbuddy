<?php


namespace App\Http;

use Request;
class Helpers {
    public static function getDistance($lat1, $lon1, $lat2, $lon2, $unit="K") {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return number_format($miles * 1.609344,1, ".", ",");
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

    public static function setActive($path)
    {
        return Request::is($path . '*') ? ' class=active' :  '';
    }

    public static function getProfileImage(\App\User $user = null)
    {
        if (env('APP_ENV') ==  "local") return "http://lorempixel.com/128/128/people/" . rand(1, 10);

        if (!$user) $user = \Auth::user();

        return $user->getFirstMediaUrl('avatar', 'thumb') ? $user->getFirstMediaUrl('avatar', 'thumb') : (string)\Avatar::create($user->name)->toBase64();
    }

    public static function format_price($value) {
        return number_format($value, 0, ',', '.') . "<small>,00</small> rsd";
    }
}