<?php

namespace App\Http\Middleware;

use Closure;

class CheckHasProfile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!\Auth::user()->address) {
            \Alert::info('Potrebno je da popunite Vaš korisnički profil pre nego što nastavite da koristite sajt.', 'Popunite profil')->autoclose(3000);
            return redirect('profile/edit');
        }
        return $next($request);
    }
}
