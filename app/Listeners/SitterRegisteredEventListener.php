<?php

namespace App\Listeners;

use App\Events\SitterRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SitterRegisteredEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SitterRegistered  $event
     * @return void
     */
    public function handle(SitterRegistered $event)
    {
        //
    }
}
