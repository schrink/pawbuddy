<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $with = ['sender'];
    protected $fillable = ['body', 'order_id', 'recipient_id', 'sender_id'];

	public function sender(  ) {
		return $this->belongsTo(\App\User::class);
    }
}
