<?php

namespace App\Notifications;

use App\Ad;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\User;

class SendPaypalNotificationCode extends Notification
{
    use Queueable;

    protected $ad;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Ad $ad)
    {
        $this->ad = $ad;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting("Dobar dan!")
            ->subject("Verifikujte Vaš PayPal email")
            ->line('Da biste mogli da  primate uplate, molimo Vas da verifikujete Vašu PayPal email adresu.')
            ->action('Verifikacija', route('paypal.email-verification.check', $this->ad->paypal_verification_token) . '?email=' .
                urlencode($this->ad->email))
            ->line('Hvala Vam što koristite našu aplikaciju!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'user_id' => $this->ad->user_id,
            'message' => 'PayPal adresa zahteva verifikaciju. Email Vam je poslat',
            'class' => 'fa fa-warning text-yellow'
        ];
    }
}
