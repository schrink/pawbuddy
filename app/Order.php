<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['ad_id', 'user_id', 'service_id', 'from_date', 'to_date', 'no_of_days', 'no_of_dogs', 'pick_up_drop_off', 'holidays', 'more_days_discount', 'net_price', 'gross_price', 'verified_stay', 'dogs'];

    protected $casts = [
        'dogs' => 'array'
    ];
    protected $with = ['customer', 'service','status', 'messages'];

	public function customer() {
		return $this->belongsTo(\App\User::class, 'user_id');
    }
	public function service() {
		return $this->belongsTo(\App\Service::class);
	}

	public function messages() {
		return $this->hasMany(\App\Message::class);
	}


	public function sitter() {
		return $this->belongsTo(\App\Ad::class, 'ad_id');
	}

	public function status(){
		return $this->belongsTo(\App\OrderStatus::class);
	}


	public function getDogsAttribute($dogs) {
		return \App\Pet::whereIn('id', json_decode($dogs))->get();
    }
}
