<?php

namespace App;

use Carbon\Carbon;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;

/**
 * Class Pet
 * @package App\Models
 * @version January 29, 2017, 6:17 pm UTC
 */
class Pet extends Model implements HasMediaConversions
{
    use HasMediaTrait;

    public $table = 'pets';
    protected $with = ['media', 'breed'];
    protected $appends = ['avatar'];


    protected $dates = ['deleted_at', 'birthday'];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'animal_type_id' => 'integer',
        'breed_id' => 'integer',
        'name' => 'string',
        'age' => 'string',
        'gender' => 'enum',
        'social' => 'integer',
        'vaccinated' => 'enum',
        'sterilised' => 'enum',
        'healthy' => 'enum',
        'healthy_description' => 'text',
        'birthday' => 'date',
    ];

    public $fillable = [
        'user_id',
        'animal_type_id',
        'name',
        'breed_id',
        'age',
        'gender',
        'social',
        'sterilised',
        'vaccinated',
        'healthy',
        'healthy_description',
        'birthday'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'animal_type' => 'required',
        'breed' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }

    public function animal_type()
    {
        return $this->belongsTo(\App\AnimalType::class);
    }

    public function breed()
    {
        return $this->belongsTo(\App\Breed::class);
    }

    public function registerMediaConversions()
    {
        $this->addMediaConversion('thumb')
            ->fit('crop', '340', '340')
            ->sharpen(10);
    }

    public function getAgeAttribute($value)
    {
        return Carbon::parse($this->birthday)->diff(Carbon::now())->format('%y godina, %m meseci');
    }

    public function getAvatarAttribute()
    {
        return $this->getFirstMediaUrl('avatar', 'thumb') ? $this->getFirstMediaUrl('avatar', 'thumb') : "/img/default-profile.jpg";
       // return $this->getFirstMediaUrl('avatar', 'thumb');
    }
}
