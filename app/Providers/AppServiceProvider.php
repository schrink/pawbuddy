<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use View;
use Illuminate\Contracts\Auth\Guard;
use Laravel\Dusk\DuskServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Guard $auth)
    {
        Schema::defaultStringLength(191);
        view()->composer(['front._menu', 'front.me'], function($view) use ($auth){
            $view->with('user', $auth->user()); // does what you expect
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() == 'local') {
            $this->app->register(\Barryvdh\Debugbar\ServiceProvider::class);
            $this->app->register('Laracasts\Generators\GeneratorsServiceProvider');
        }

        if ($this->app->environment('local', 'testing'))
        {
            $this->app->register(DuskServiceProvider::class);
        }

        $this->app->singleton(\Faker\Generator::class, function () {
            return \Faker\Factory::create('sr_Latn_RS');
        });


    }
}
