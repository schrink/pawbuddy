<?php

namespace App\Repositories;

use App\Pet;
use InfyOm\Generator\Common\BaseRepository;

class PetRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'breed',
        'age',
        'gender',
        'social',
        'vaccinated',
        'healthy',
        'illness'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Pet::class;
    }
}
