<?php

namespace App;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Service
 * @package App\Models
 * @version January 29, 2017, 6:17 pm UTC
 */
class Service extends Model
{

    public $table = 'services';
    
    public $timestamps = false;
    protected $dates = ['deleted_at'];
    protected $appends = ['willDo'];


    public $fillable = [
        'title'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'title' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function ads()
    {
        return $this->belongsToMany(\App\Ad::class);
    }

    /* Custom attributes */

    public function getWillDoAttribute(){
        switch($this->id) {
	        case 1:
	        	return __('will care about your pet at own home while you are at trip');
	        	break;
	        case 2:
		        return __('will take your dog for a walk for an hour');
		        break;
	        case 3:
		        return __('will care about your pet at own home while you are at work');
		        break;
        }
    }
}
