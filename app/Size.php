<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    protected $fillable = ['title', 'weight', 'size'];
    protected $cast = ['title' => 'string'];
    public $timestamps = false;

    public function ads()
    {
        return $this->belongsToMany(\App\Ad::class);
    }

}
