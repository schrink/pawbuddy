<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public $timestamps = false;
    protected $with = ['category'];

	/**
	 * Get all of the ads that are assigned this tag.
	 */
	public function ads()
	{
		return $this->morphedByMany('App\Ad', 'taggable');
	}

	/**
	 * Get all of the services that are assigned this tag.
	 */
	public function services()
	{
		return $this->morphedByMany('App\AdService', 'taggable');
	}

	public function category(){
		return $this->belongsTo( 'App\TagCategory', 'tag_category_id');
	}
}
