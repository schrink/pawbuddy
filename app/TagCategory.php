<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TagCategory extends Model
{
	public $timestamps = false;
	protected $fillable = ['title'];

    public function tags() {
    	return $this->hasMany(\App\Tag::class);
    }
}
