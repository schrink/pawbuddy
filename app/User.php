<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
//use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Laravel\Passport\HasApiTokens;
use BrianFaust\Reviewable\HasReviews;
use TCG\Voyager\Models\Role;

class User extends Authenticatable implements HasMediaConversions
{
    use HasMediaTrait, HasApiTokens;
    use Notifiable, HasReviews;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $with = ['media', 'reviews', 'roles', 'pets'];
    protected $appends = ['rating', 'avatar', 'firstname'];
    protected $fillable = [
        'name', 'email', 'password', 'city', 'country', 'address', 'birthdate', 'emergency_phone', 'emergency_name', 'gender', 'personal_id', 'lat_lng'
    ];

    protected $casts = [
        'lat_lng' => 'array'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = [
      'created_at', 'birthdate'
    ];

    public function roles()
    {
        return $this->belongsTo(Role::class);
    }
    public function ad()
    {
        return $this->hasOne(\App\Ad::class);
    }

	public function orders() {
		return $this->hasMany(\App\Order::class);
    }
    public function pets()
    {
        return $this->hasMany(\App\Pet::class);
    }
    public function registerMediaConversions()
    {
        $this->addMediaConversion('thumb')
            ->fit('crop', 340, 340)
            ->sharpen(10);
    }

    public function getRating(): float
    {
        return round($this->reviews->avg('rating'), 1);
    }

    /*
     * Custom attributes
     */

    public function getRatingAttribute()
    {
        return $this->getRating();
    }

    public function getAvatarAttribute()
    {
        return \Helpers::getProfileImage($this);
    }
	public function getFirstnameAttribute()
	{
		$exp = explode(" ", $this->name);
		return $exp[0];
	}
}
