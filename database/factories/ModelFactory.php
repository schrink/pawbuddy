<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker)
{
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'city' => $faker->city,
        'country' => 'RS',
        'gender' => $faker->optional()->randomElement(['male', 'female']),
        'address' => $faker->address,
        'phone' => $faker->phoneNumber,
        'phone_verified' => $faker->numberBetween(0,1),
        'emergency_phone' => $faker->phoneNumber,
        'emergency_name' => $faker->firstName,
        'birthdate' => $faker->date('Y-m-d'),
        'verified' => 1,
        'lat_lng' => ['lat' => $faker->latitude(44.70, 44.86) , 'lng' => $faker->longitude(20.34, 20.61) ],
    ];
});

$factory->define(App\Pet::class, function (Faker\Generator $faker)
{
    return [
        'user_id' => $faker->numberBetween(1, 100),
        'animal_type_id' => $faker->numberBetween(1, 3),
        'name' => $faker->firstName,
        'breed_id' => $faker->numberBetween(1, 299),
        'birthday' => $faker->date('Y-m-d'),
        'gender' => $faker->optional()->randomElement(['female', 'male']),
        'social' => $faker->numberBetween(1, 5),
        'sterilised' => $faker->optional()->randomElement(['yes', 'no']),
        'vaccinated' => $faker->optional()->randomElement(['yes', 'no']),
        'healthy' => $faker->optional()->randomElement(['yes', 'no']),
        'healthy_description' => $faker->paragraph(3),
    ];
});



$factory->define(App\Ad::class, function (Faker\Generator $faker)
{
    return [
        'published' => $faker->numberBetween(0, 1),
        'title' => $faker->sentence(3),
        'about' => $faker->paragraphs(3, true),
        'about_home' => $faker->paragraphs(2, true),
        'residence' => $faker->optional()->randomElement(["house","apartment"]),
        'kids' => $faker->optional()->randomElement(["yes","no"]),
        'car' => $faker->optional()->randomElement(["yes","no"]),
        //'other_pets' => $faker->optional()->randomElements(["Psi","Mačke","Glodari","Zečevi","Ptice","Reptili"], rand(1, 6)),
        'experience' => $faker->numberBetween(1, 20),
        'cynologist' => $faker->optional()->randomElement(["yes","no"]),
        'veterinary' =>$faker->optional()->randomElement(["yes","no"]),

        'personal_id' => $faker->ean13
    ];
});

//$factory->define(App\BusyDay::class, function (Faker\Generator $faker)
//{
//    return [
//        'from_date' => $faker->date(),
//        'to_date' => $faker->numberBetween(1, 3),
//    ];
//});

$factory->define(App\Order::class, function (Faker\Generator $faker)
{

    $date = $faker->dateTimeThisYear();
    $days = rand(0, 7);
    $hollidays = $faker->numberBetween(0, 2);
    $to_date = \Carbon\Carbon::instance($date)->addDays($days + $hollidays);
    $ad = $faker->numberBetween(8, 100);
    $service = $faker->numberBetween(1, 3);
    $adService = \App\AdService::where('ad_id', $ad)->first();

    $no_of_dogs = $faker->numberBetween(1, 4);
    $normal_days_price = $days * $adService->price * $no_of_dogs;
    $holiday_price = $hollidays * $adService->holiday_price * $no_of_dogs;
    try{
        $withDiscount = ($normal_days_price + $holiday_price) * (1 - ($adService->more_days_discount / 100));
        $total = $withDiscount;
    } catch (Exception $e) {

        $total = $holiday_price + $normal_days_price;
    }

    $pick = $faker->randomElement(['yes', 'no']);
    if($pick == "yes") {
        $total += $adService->pick_up_drop_off;
    }
    $grandTotal = $total + 150;
    return [
        'status_id' => $faker->numberBetween(1, 4),
        'ad_id' => $ad,
	    'user_id' => $faker->numberBetween(1, 100),
	    'dogs' => [1, 2, 3],
        'service_id' => $service,
        'from_date' => $faker->date(),
        'to_date' => $to_date,
        'no_of_days' => $days,
        'no_of_dogs' => $no_of_dogs,
        'pick_up_drop_off' => $pick,
        'holidays' => $hollidays,
        'more_days_discount' => $days > 3 ? 'yes' : 'no',
        'net_price' => $total,
        'gross_price' => $grandTotal,
        'verified_stay' => $faker->randomElement(['yes', 'no'])

        //'user_id' => $faker->numberBetween(1, 100),
    ];
});

$factory->define(App\Message::class, function (Faker\Generator $faker)
{
    return [
        'order_id' => $faker->numberBetween(1, 100),
        'sender_id' => $faker->numberBetween(1, 100),
        'recipient_id' => $faker->numberBetween(1, 100),
        'not_read' => $faker->numberBetween(0, 1),
	    'body' => $faker->paragraph()

    ];
});