<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePetsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('pets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('name');
            $table->integer('animal_type_id')->unsigned();
            $table->integer('breed_id')->unsigned();
            $table->date('birthday')->nullable();
            $table->enum('gender', ['female', 'male'])->nullable();
            $table->integer('social')->nullable();
            $table->enum('sterilised', ['no', 'yes'])->nullable();
            $table->enum('vaccinated', ['no', 'yes'])->nullable();
            $table->enum('healthy', ['no', 'yes'])->nullable();
            $table->text('healthy_description')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('breed_id')->references('id')->on('breeds');
            $table->foreign('animal_type_id')->references('id')->on('animal_types');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pets');
    }
}
