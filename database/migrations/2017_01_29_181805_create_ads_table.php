<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('ads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->boolean('published')->default(false);
            $table->integer('status_id')->unsigned()->default(1);
            $table->string('title')->nullable();;
            $table->string('slug')->nullable();
            $table->text('about')->nullable();;
            $table->enum('residence', ['house', 'apartment'])->nullable();
            $table->enum('kids', ['no', 'yes'])->nullable();
            $table->enum('car', ['no', 'yes'])->nullable();
            $table->integer('experience')->nullable();
            $table->enum('cynologist', ['no', 'yes'])->nullable();
            $table->enum('veterinary', ['no', 'yes'])->nullable();
            $table->text('about_home')->nullable();
            $table->string('personal_id')->nullable();
            $table->string('paypal_email')->nullable();
            $table->string('paypal_name')->nullable();
            $table->string('paypal_lastname')->nullable();
            $table->string('paypal_verification_token')->nullable();
            $table->boolean('paypal_verified')->nullable();
            $table->enum('tos', ['no', 'yes'])->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('status_id')->references('id')->on('ad_statuses')->onDelete('cascade');


        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::drop('ads');
        Schema::enableForeignKeyConstraints();
    }
}
