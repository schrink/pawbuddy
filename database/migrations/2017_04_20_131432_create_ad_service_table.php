<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_service', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('ad_id')->unsigned();
            $table->foreign('ad_id')->references('id')->on('ads')->onDelete('cascade');

            $table->integer('service_id')->unsigned()->nullable();
            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');

            $table->integer('price')->nullable();
            $table->integer('holiday_price')->nullable();
            $table->integer('additional_dog_price')->nullable();
            $table->integer('pick_up_drop_off')->nullable();
            $table->integer('more_days_discount')->nullable();
            $table->integer('max_no_of_dogs')->nullable();
            $table->json('days')->nullable();
            $table->json('times')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ad_service');
    }
}
