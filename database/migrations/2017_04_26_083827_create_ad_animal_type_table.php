<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdAnimalTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_animal_type', function (Blueprint $table) {
            $table->integer('ad_id')->unsigned();
            $table->integer('animal_type_id')->unsigned();

            $table->foreign('ad_id')->references('id')->on('ads')->onDelete('cascade');
            $table->foreign('animal_type_id')->references('id')->on('animal_types')->onDelete('cascade');
            $table->primary(['ad_id', 'animal_type_id']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ad_animal_type');
    }
}
