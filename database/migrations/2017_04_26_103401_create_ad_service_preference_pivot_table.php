<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdServicePreferencePivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_service_preference', function (Blueprint $table) {
            $table->integer('ad_service_id')->unsigned()->index();
            $table->foreign('ad_service_id')->references('id')->on('ad_service')->onDelete('cascade');
            $table->integer('preference_id')->unsigned()->index();
            $table->foreign('preference_id')->references('id')->on('preferences')->onDelete('cascade');
            $table->primary(['ad_service_id', 'preference_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ad_service_preference');
    }
}
