<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('status_id')->indexed();
            $table->integer('ad_id')->unsigned();
            $table->foreign('ad_id')->references('id')->on('ads')->onDelete('cascade');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('service_id')->unsigned();
            $table->date('from_date');
            $table->date('to_date');
            $table->integer('no_of_days');
            $table->integer('no_of_dogs');
            $table->json('dogs');
            $table->enum('pick_up_drop_off', ['no', 'yes']);
            $table->integer('holidays');
            $table->enum('more_days_discount', ['no', 'yes']);
            $table->integer('net_price');
            $table->integer('gross_price');
            $table->enum('verified_stay', ['no', 'yes']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
