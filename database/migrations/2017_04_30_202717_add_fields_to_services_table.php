<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('services', function (Blueprint $table) {
            $table->dropTimestamps();
            $table->string('icon')->default('pe-7s-home');
            $table->text('description')->nullable();
            $table->string('color')->nullable();
            $table->string('slug')->nullable();
            $table->enum('has_terms', ['no', 'yes'])->default('no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('services', function (Blueprint $table) {
            $table->timestamps();
            $table->dropColumn('icon');
            $table->dropColumn('description');
            $table->dropColumn('color');
            $table->dropColumn('has_terms');
            $table->dropColumn('slug');
        });
    }
}
