<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Eloquent::unguard();
	    DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::create('messages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->integer('sender_id')->unsigned();
            $table->integer('recipient_id')->unsigned();
            $table->boolean('not_read')->default(1);
            $table->text('body');
            $table->timestamps();
	        $table->foreign('sender_id')->references('id')->on('users');
	        $table->foreign('recipient_id')->references('id')->on('users');
	        $table->foreign('order_id')->references('id')->on('orders');

        });
	    DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
