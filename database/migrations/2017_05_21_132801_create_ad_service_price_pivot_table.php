<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdServicePricePivot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_service_price', function (Blueprint $table) {
            $table->integer('ad_service_id')->unsigned()->index();
            $table->foreign('ad_service_id')->references('id')->on('ad_services')->onDelete('cascade');
            $table->integer('price_id')->unsigned()->index();
            $table->foreign('price_id')->references('id')->on('prices')->onDelete('cascade');
            $table->primary(['ad_service_id', 'price_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ad_service_price');
    }
}
