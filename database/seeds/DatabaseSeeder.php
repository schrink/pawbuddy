<?php

use App\AdStatus;
use App\Allowance;
use App\AnimalType;
use App\Badge;
use App\Breed;
use App\Age;
use App\OrderStatus;
use App\Size;
use App\Preference;
use App\HomeFeature;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
    public function prepareArray($ar, $no=3)
    {
        $original = $ar;
        //$keys = array_keys($ar);
        //shuffle($keys);
//        foreach ($keys as $key)
//        {
//            $new[$key] = $ar[$key];
//
//        }
        shuffle($ar);
       // $ar = $new;
        //var_dump($ar);
        $ar = array_slice($ar, 0, rand(1, $no), true);
        ksort($ar);
        return json_encode($ar);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        AdStatus::create(['status' => 'Nje započeto']);
        AdStatus::create(['status' => 'Na čekanju']);
        AdStatus::create(['status' => 'Odobreno']);

        $this->call(UsersTableSeeder::class);
        $this->call(ServicesSeeder::class);
        Model::unguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        // $this->call(UserTableSeeder::class);
        AnimalType::truncate();
        AnimalType::create(['type' => 'Psi', 'lang' => 'rs']);
        AnimalType::create(['type' => 'Mačke', 'lang' => 'rs']);
        AnimalType::create(['type' => 'Glodari', 'lang' => 'rs']);
        AnimalType::create(['type' => 'Zečevi', 'lang' => 'rs']);
        AnimalType::create(['type' => 'Ptice', 'lang' => 'rs']);
        AnimalType::create(['type' => 'Reptili', 'lang' => 'rs']);
        AnimalType::create(['type' => 'Drugo', 'lang' => 'rs']);

        Age::create(['title' => 'Štene', 'months' => 0, 'description' => 'do 12m', 'icon' => 'fa-edit']);
        Age::create(['title' => 'Odrastao', 'months' => 12, 'description' => 'do 8g', 'icon' => 'fa-edit']);
        Age::create(['title' => 'Stariji', 'months' => 100, 'description' => '8g+', 'icon' => 'fa-edit']);

        Size::create(['title' => 'Mini', 'weight' => '0 - 3', 'icon' => 'fa-edit']);
        Size::create(['title' => 'Mali', 'weight' => '4 - 7', 'icon' => 'fa-edit']);
        Size::create(['title' => 'Srednji', 'weight' => '8 - 20', 'icon' => 'fa-edit']);
        Size::create(['title' => 'Veliki', 'weight' => '21 - 40', 'icon' => 'fa-edit']);
        Size::create(['title' => 'Giant', 'weight' => '40+', 'icon' => 'fa-edit']);

        Allowance::create(['title' => 'Dozvoljeno da se penje na nameštaj']);
        Allowance::create(['title' => 'Dozvoljeno da se penje na krevet']);

        Preference::create(['title' => 'Bez ženki u teranju']);
        Preference::create(['title' => 'Sterilizovani i kastrirani psi']);

        HomeFeature::create(['title' => 'Siter živi na farmi']);
        HomeFeature::create(['title' => 'Postoji ograđeno dvorište']);
        HomeFeature::create(['title' => 'U blizini se nalazi park']);

        OrderStatus::create(['status'=> 'Čeka odgovor']);
        OrderStatus::create(['status'=> 'Prihvaćeno']);
        OrderStatus::create(['status'=> 'Odbijeno']);
        OrderStatus::create(['status'=> 'Završeno']);




        Badge::create(['title' => 'Ponovljena usluga', 'event' => 'RepeatingCustomerEvent', 'description' => 'Imali smo dva puta narudžbinu od istog naručioca']);


        Breed::truncate();
        //factory(Breed::class, 30)->create();
        $this->call(BreedsTableSeeder::class);
        $this->call(VoyagerDatabaseSeeder::class);
        $this->call(SettingsTableSeeder::class);

        /* SEEDERS FOR TESTING ONLY */


        $days = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
        $times = ['1', '2', '3'];

        factory(\App\User::class, 100)->create()->each(function ($u) use ($days, $times)
        {
            for($i=0; $i < rand(0, 10); $i++)
            {
                $faker = Faker\Factory::create();
                $rateUser = \App\User::inRandomOrder()->first();
                $u->createReview([
                    'title' => $faker->sentence(3),
                    'body' => $faker->paragraph(3),
                    'rating' => $faker->numberBetween(1, 5),
                ], $rateUser);
            }
            $ad = $u->ad()->save(factory(\App\Ad::class)->make());
            if($ad->id > 7 && $ad->id < 17) {
            	for($i=0; $i<3; $i++){
		            $faker = Faker\Factory::create();
		            $ad->addMedia($faker->image())
			            ->toMediaLibrary('images');
	            }
            }

            for($i=0; $i < rand(0, 10); $i++)
            {
                $faker = Faker\Factory::create();
                $rateUser = \App\User::inRandomOrder()->first();
                $ad->createReview([
                    'title' => $faker->sentence(3),
                    'body' => $faker->paragraph(3),
                    'rating' => $faker->numberBetween(1, 5),
                ], $rateUser);
            }

            $ar = [
                1 => ['price' => rand(150, 2000), 'holiday_price' => rand(150, 2000), 'additional_dog_price' => rand(0, 30), 'pick_up_drop_off' => rand(150, 500), 'more_days_discount' => rand(0, 30), 'max_no_of_dogs' => rand(1, 5), 'days' => $this->prepareArray($days, 7), 'times' => $this->prepareArray($times)],
                2 => ['price' => rand(150, 2000), 'holiday_price' => rand(150, 2000), 'additional_dog_price' => rand(0, 30), 'pick_up_drop_off' => rand(150, 500), 'more_days_discount' => rand(0, 30), 'max_no_of_dogs' => rand(1, 5), 'days' => $this->prepareArray($days,7), 'times' => $this->prepareArray($times)],
                3 => ['price' => rand(150, 2000), 'holiday_price' => rand(150, 2000), 'additional_dog_price' => rand(0, 30), 'pick_up_drop_off' => rand(150, 500), 'more_days_discount' => rand(0, 30), 'max_no_of_dogs' => rand(1, 5), 'days' => $this->prepareArray($days,7), 'times' => $this->prepareArray($times)]
            ];
            $keys = array_keys($ar);
            shuffle($keys);
            foreach ($keys as $key)
            {
                $new[$key] = $ar[$key];

            }
            $ar = $new;
            $ar = array_slice($ar, 0, rand(1, 3), true);
            $ad->services()->sync($ar);

//            $ar = [1, 2];
//            if(rand(0,1))
//            {
//                shuffle($ar);
//                $ad->preferences()->sync(array_slice($ar, 0, rand(1, 2), true));
//            }
            //other_pets
            if(rand(0,1))
            {
                $ar = [1, 2, 3, 4, 5, 6, 7];
                shuffle($ar);
                $ad->other_pets()->sync(array_slice($ar, 0, rand(1, 3), true));
            }

            //ages
            $ar = [1, 2, 3];
            if(rand(0,1)) {
                shuffle($ar);
                $ad->accepting_ages()->sync(array_slice($ar, 0, rand(1, 2), true));
            } else {
                $ad->accepting_ages()->sync($ar);
            }


            //sizes
            $ar = [1, 2, 3, 4, 5];
            if(rand(0,1))
            {
                shuffle($ar);
                $ad->sizes()->sync(array_slice($ar, 0, rand(0, 3), true));
            } else {
                $ad->sizes()->sync($ar);
            }

            //breeds to care
            if (rand(0, 1))
            {
                $ar = [1, 2, 3, 4, 5];
                shuffle($ar);
                $ad->breeds_to_care()->sync(array_slice($ar, 0, rand(0, 3), true));
            }

            $ar = [1, 2];
            if(rand(0,1))
            {
                shuffle($ar);
                $ad->allowances()->sync(array_slice($ar, 0, rand(1, 2), true));
            }



            $ar = [1, 2];
            if(rand(0,1))
            {
                shuffle($ar);
                $ad->home_features()->sync(array_slice($ar, 0, rand(1, 2), true));
            }

            if(rand(0,1))
            {
                $ad->badges()->sync([1]);
            }

        });

        $services = \App\AdService::all();
        foreach ($services as $service) {
            $ar = [1, 2];
            if(rand(0,1))
            {
                shuffle($ar);
                $service->preferences()->sync(array_slice($ar, 0, rand(1, 2), true));
            }
        }
        factory(\App\Pet::class, 150)->create();
        $adServices = \App\AdService::all();
        foreach ($adServices as $service)
        {
            factory(\App\Order::class, 1)->create(['id' => $service]);
        }

        factory(\App\Message::class, 300)->create();
        $this->call(DataRowsTableSeeder::class);
        $this->call(DataTypesTableSeeder::class);
        Model::reguard();
    }
}
