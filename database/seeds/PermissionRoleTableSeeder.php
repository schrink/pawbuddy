<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Permission;
use TCG\Voyager\Models\Role;

class PermissionRoleTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::where('name', 'admin')->firstOrFail();

        $permissions = Permission::all();

        $role->permissions()->sync(
            $permissions->pluck('id')->all()
        );

        $permissions = Permission::where('key', 'siiter_candidate')->get()->pluck('id');
        $role = Role::where('name', 'sitter-candidate')->firstOrFail();
        $role->permissions()->sync($permissions);
    }
}
