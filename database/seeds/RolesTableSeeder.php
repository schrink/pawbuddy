<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        $role = Role::firstOrNew(['name' => 'admin']);
        if (!$role->exists) {
            $role->fill([
                    'display_name' => 'Administrator',
                ])->save();
        }



        $role = Role::firstOrNew(['name' => 'user']);
        if (!$role->exists) {
            $role->fill([
                    'display_name' => 'Normal User',
                ])->save();
        }

        $role = Role::firstOrNew(['name' => 'controller']);
        if (!$role->exists) {
            $role->fill([
                'display_name' => 'Controller',
            ])->save();
        }

        $role = Role::firstOrNew(['name' => 'sitter']);
        if (!$role->exists) {
            $role->fill([
                'display_name' => 'Sitter',
            ])->save();
        }

        $role = Role::firstOrNew(['name' => 'sitter-candidate']);
        if (!$role->exists) {
            $role->fill([
                'display_name' => 'Sitter Kandidat',
            ])->save();
        }


    }
}
