<?php

use App\Service;
use Illuminate\Database\Seeder;

class ServicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Service::create(['title' => 'Čuvanje', 'slug' => 'sitting', 'icon' => 'pe-7s-home', 'color' => 'red', 'has_terms' => 'no', 'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aliquam assumenda debitis, deleniti earum excepturi expedita facilis harum hic magni maiores nobis omnis possimus provident quidem quos suscipit veniam vitae!']);
        Service::create(['title' => 'Šetnja', 'slug' => 'walking', 'icon' => 'pe-7s-way', 'color' => 'green', 'has_terms' => 'yes', 'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aliquam assumenda debitis, deleniti earum excepturi expedita facilis harum hic magni maiores nobis omnis possimus provident quidem quos suscipit veniam vitae!']);
        Service::create(['title' => 'Dnevno', 'slug' => 'daysitting', 'icon' => 'ion-ios-paw-outline', 'color' => 'blue', 'has_terms' => 'no', 'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aliquam assumenda debitis, deleniti earum excepturi expedita facilis harum hic magni maiores nobis omnis possimus provident quidem quos suscipit veniam vitae!']);
    }
}
