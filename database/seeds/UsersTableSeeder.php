<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
	    $user = \App\User::create(['name' => 'Vladimir Lelicanin', 'email' => 'vlelicanin@strategicpoint.rs', 'password' =>
		    \Hash::make('admin123'), 'address' => 'Marsala Birjuzova 32', 'lat_lng' => ['lat' => 44.815753,'lng' => 20.4563614], 'role_id' => 1, 'verified' => 1]);

        
        \DB::table('users')->insert(array (
            0 => 
            array (

                'role_id' => 2,
                'name' => 'Sava Bezanovic',
                'email' => 'savabezanovic@hotmail.com',
                'avatar' => 'users/default.png',
                'password' => '$2y$10$FLF0nMxokfjugejL3.Lr6OT7/A92kP1sftPQyfmCLmfFtKn5vQeo2',
                'city' => 'Indjija',
                'country' => 'Srbija',
                'gender' => 'male',
                'birthdate' => '1997-01-09',
                'phone' => '0691847057',
                'address' => 'Kralja Petra 47A',
                'lat_lng' => '{"lat":45.044272,"lng":20.0847273}',
                'emergency_phone' => '0631847057',
                'emergency_name' => 'Vesna Marceta',
                'verified' => 1,
                'verification_token' => NULL,
                'phone_verified' => NULL,
                'phone_verification_token' => NULL,
                'remember_token' => '2NUNcaPKWCxSlk2I9tl3Ix0JjnHV6btLLEffkrSdTb8OeuvPdD5UggQ1V4k7',
                'created_at' => '2017-05-09 06:08:33',
                'updated_at' => '2017-05-09 06:13:06',
            ),
            1 => 
            array (

                'role_id' => 5,
                'name' => 'Bojan Voves',
                'email' => 'bojan.voves@gmail.com',
                'avatar' => 'users/default.png',
                'password' => '$2y$10$PH9FzxgQXh9T2wfOgIeqGeSYmTy5PT8trG4VdZaNTOGhYtS5EVWsy',
                'city' => 'Belgrade',
                'country' => 'Serbia',
                'gender' => 'male',
                'birthdate' => '1980-08-01',
                'phone' => '631821588',
                'address' => 'Patrijaha Gavrila 2, 8',
                'lat_lng' => '{"lat":44.804794,"lng":20.4752077}',
                'emergency_phone' => '631821588',
                'emergency_name' => 'Tijana',
                'verified' => 1,
                'verification_token' => NULL,
                'phone_verified' => NULL,
                'phone_verification_token' => NULL,
                'remember_token' => NULL,
                'created_at' => '2017-05-09 10:04:56',
                'updated_at' => '2017-05-09 15:12:16',
            ),
            2 => 
            array (

                'role_id' => 5,
                'name' => 'Boris Jankovic',
                'email' => 'solarsistem@gmail.com',
                'avatar' => 'users/default.png',
                'password' => '$2y$10$M2AvxX/l.rgQUQGakvrABe.l27pDVMvtEuZFB3pcx4FRdO/oYXIcu',
                'city' => 'Belgrade',
                'country' => 'Serbia',
                'gender' => 'male',
                'birthdate' => '2013-09-04',
                'phone' => '069664995',
                'address' => 'Ohridska 5/60',
                'lat_lng' => '{"lat":44.8449489,"lng":20.3975891}',
                'emergency_phone' => '069664995',
                'emergency_name' => 'Boris Jankovic',
                'verified' => 1,
                'verification_token' => NULL,
                'phone_verified' => NULL,
                'phone_verification_token' => NULL,
                'remember_token' => NULL,
                'created_at' => '2017-05-09 11:28:05',
                'updated_at' => '2017-05-09 12:02:25',
            ),
            3 => 
            array (
                'role_id' => 2,
                'name' => 'Aljosa Dragovic',
                'email' => 'aljosaaka98@gmail.com',
                'avatar' => 'users/default.png',
                'password' => '$2y$10$EZn9WmXR.o97J3Bx72nDbeQJObHxqreio5KVZ5P1LpgmGHxRwU7OG',
                'city' => 'Beograd',
                'country' => 'Srbija',
                'gender' => 'male',
                'birthdate' => '2017-01-12',
                'phone' => '0617151417',
                'address' => 'Ulica',
                'lat_lng' => '{"lat":44.8107722,"lng":20.343102}',
                'emergency_phone' => '0617151417',
                'emergency_name' => 'Pera',
                'verified' => 1,
                'verification_token' => NULL,
                'phone_verified' => NULL,
                'phone_verification_token' => NULL,
                'remember_token' => 'zhBVNwukVTXLtX8AhtRT1Z9OhaUCUjBrvG26COGVSra5jkNPp7Q0LLj1mKRt',
                'created_at' => '2017-05-09 22:24:08',
                'updated_at' => '2017-05-09 22:30:08',
            ),
            4 => 
            array (

                'role_id' => 5,
                'name' => 'Marko Peric',
                'email' => 'marcko.peric@gmail.com',
                'avatar' => 'users/default.png',
                'password' => '$2y$10$eQTDGZiWdeG.nF2m8qxgLOXx0pkrtSWwCVCvvoLxBST.4GUOfVPQS',
                'city' => 'beograd',
                'country' => 'Srbija',
                'gender' => 'male',
                'birthdate' => '2017-05-12',
                'phone' => '0612223344',
                'address' => 'Makedonska 23',
                'lat_lng' => '{"lat":44.8155544,"lng":20.4638522}',
                'emergency_phone' => '2313123123',
                'emergency_name' => 'Perica',
                'verified' => 1,
                'verification_token' => NULL,
                'phone_verified' => NULL,
                'phone_verification_token' => NULL,
                'remember_token' => NULL,
                'created_at' => '2017-05-11 06:52:48',
                'updated_at' => '2017-05-11 07:20:18',
            ),
            5 => 
            array (
                'role_id' => 2,
                'name' => 'Milos Tadic',
                'email' => 'x3nex@hotmail.com',
                'avatar' => 'users/default.png',
                'password' => '$2y$10$0I2lphrkPX/IJP3N80nbeeb4nB7/GgkNUWhyDE/jvBx0.UMctnAKq',
                'city' => NULL,
                'country' => NULL,
                'gender' => NULL,
                'birthdate' => NULL,
                'phone' => NULL,
                'address' => NULL,
                'lat_lng' => NULL,
                'emergency_phone' => NULL,
                'emergency_name' => NULL,
                'verified' => 1,
                'verification_token' => NULL,
                'phone_verified' => NULL,
                'phone_verification_token' => NULL,
                'remember_token' => NULL,
                'created_at' => '2017-05-16 14:08:57',
                'updated_at' => '2017-05-16 14:08:57',
            ),
        ));
        $users = \App\User::all();
        foreach ($users as $user) {
	        $user->ad()->create([]);
        }
        
    }
}