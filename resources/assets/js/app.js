/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('jquery-slimscroll');
require('fastclick');
require('daterangepicker');
require('select2');
require('sweetalert');
// require('tinymce');
// require('../../../node_modules/tinymce/themes/modern/theme.js');
import {VueForm, Event} from 'vue-form-2';
import VueCoreImageUpload  from 'vue-core-image-upload';
import VueDropzone from 'vue2-dropzone';
import VueModal from 'vue2-bootstrap-modal';
import VueLightbox from 'vue-lightbox'
Vue.component("Lightbox",VueLightbox)
//Vue.use(VueCoreImageUpload)
Vue.use(VueForm)
import Carousel3d from 'vue-carousel-3d'

Vue.use(Carousel3d);

// var VueValidator = require('vue-validator')
//
// Vue.use(VueValidator)

import * as VueGoogleMaps from 'vue2-google-maps';

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyA-s0zX2A9Y37ocXb7VVkhbS_2SDrZnZn8',
  }
});
import StarRating from 'vue-star-rating'
Vue.component('star-rating', StarRating);
import Datepicker from 'vuejs-datepicker';
Vue.component('datepicker', Datepicker);

// var datepicker = require('vue-strap').datepicker;
// Vue.component(datepicker)
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Vue.component('example', require('./components/Example.vue'));
// Vue.component('display-profile-image', require('./components/ProfileImage.vue'));
// Vue.component('animal-select', require('./components/AnimalSelect.vue'));
// Vue.component('dropzone', VueDropzone);
// Vue.component('images-upload', require('./components/ImagesUpload.vue'));
// Vue.component('conditional-input-with-upload', require('./components/ConditionalInputWithUpload.vue'));
// Vue.component('display-media', require('./components/DisplayMedia.vue'));
// Vue.component('modal', VueModal);
// Vue.component('service-price-input', require('./components/ServicePriceInput.vue'))
// Vue.component('upload-images', require('./components/UploadImages.vue'));
// Vue.component('image-with-delete', require('./components/ImageWithAjaxDelete.vue'));
Vue.component('gmap', require('./components/Map.vue'));

const app = new Vue({
  el: '#app',
  mounted(){

  },
  components: {
    'vue-core-image-upload': VueCoreImageUpload,
  },

  data: {
  },
  methods: {
    imageuploaded(res) {
      if (res.errcode == 0) {
        Events.$emit('profile-image-changed', res.file);
      }
    },
    imageuploading(res) {
      console.info('uploading');
    },
    errorhandle(err) {
      console.error(err);
    }
  }
});
