require('../../bootstrap');

const app = new Vue({
  el: '#app',
  components: {
    'my-profile' : require('../../components/MyProfile.vue'),
  },
  data: {

  },
  mounted(){
    this.$refs.app.style.display = 'block'

  }
});