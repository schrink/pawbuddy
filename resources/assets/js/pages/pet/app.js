require('../../bootstrap');

const app = new Vue({
    el: '#app',
    components: {
        'pet-create-form' : require('../../components/pet/PetCreateForm.vue'),
    },
    data: {

    },
    mounted(){
        this.$refs.app.style.display = 'block'

    }
});