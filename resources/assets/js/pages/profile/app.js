require('../../bootstrap');

import Datepicker from 'vuejs-datepicker';

import {VueForm, Event} from 'vue-form-2';
Vue.use(VueForm);

const app = new Vue({
  el: '#app',
  components: {
    'date-picker' : Datepicker,
    'user-box' : require('../../components/UserBox.vue'),
    'display-profile-image' : require('../../components/ProfileImage.vue'),
    'vue-core-image-upload' : require('vue-core-image-upload'),

  },
  mounted(){
   // this.$refs.app.style.display = 'block'
  },
  methods: {
    imageuploaded(res) {
      if (res.errcode == 0) {
        Events.$emit('profile-image-changed', res.file);
      }
    },
    imageuploading(res) {
      console.info('uploading');
    },
    errorhandle(err) {
      console.error(err);
    }
  }
});