require('../../bootstrap');

const app = new Vue({
  el: '#app',
  components: {
    'sitter-profile' : require('../../components/SitterProfile.vue'),
    'contact-sitter-form' : require('../../components/sitter-contact/ContactSitterForm.vue'),
    'user-box' : require('../../components/UserBox.vue'),
    'calculate-prices' : require('../../components/sitter-contact/CalcuatePrices.vue'),
    'display-media' : require('../../components/sitter-profile/DisplayImages.vue')
  },
  mounted(){
    this.$refs.app.style.display = 'block'
  }
});