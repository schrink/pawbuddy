require('../../bootstrap');
require('select2');
//import Datepicker from 'vuejs-datepicker';

import {VueForm, Event} from 'vue-form-2';
Vue.use(VueForm);


const app = new Vue({
  el: '#app',
  components: {

    'star-rating' : require('vue-star-rating'),
    'conditional-input-with-upload' : require('../../components/ConditionalInputWithUpload.vue'),
    'image-with-delete' : require('../../components/ImageWithAjaxDelete.vue'),
    'upload-images' : require('../../components/UploadImages.vue'),
    'feature-switch' : require('../../components/FeatureSwitch.vue'),
    'app-button' : require('../../components/AppButton.vue'),
    'services-select' : require('../../components/ServicesSelection.vue')
  },
  data: {

  },
  mounted(){
    this.$refs.app.style.display = 'block'

  }
});