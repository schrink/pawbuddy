require('../../bootstrap');

const app = new Vue({
  el: '#app',
  components: {
    'star-rating' : require('vue-star-rating'),
    'gmap': require('../../components/Map.vue'),
    'search-form' : require('../../components/SearchForm.vue'),
    'ads-display' : require('../../components/AdsDsiplay.vue')
  },
  mounted(){
    this.$refs.app.style.display = 'block'
  }
});