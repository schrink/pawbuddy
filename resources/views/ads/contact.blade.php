@extends('front.app')

@section('content')
    <div class="container" id="app" ref="app">
        <div class="row top-margin-30">
            <div class="col-md-7">
                <div class="box box-widget widget-user-2">
                    <div class="widget-user-header bg-navy">
                        <div class="widget-user-image">
                            <img class="img-circle" src="{{$ad->user->avatar}}" alt="User Avatar">
                        </div>
                        <!-- /.widget-user-image -->
                        <h3 class="widget-user-username">{{$ad->user->name}}</h3>
                        <h5 class="widget-user-desc">{{$ad->user->city}}</h5>
                    </div>

                    <div class="box-body  top-margin-30">
                        <contact-sitter-form
                                ad-data="{{json_encode($ad)}}"
                                pets="{{json_encode(\Auth::user()->pets)}}"
                                animal-types="{{$animalTypes}}"
                                dog-breeds="{{$dogs}}"
                                cats="{{$cats}}"
                                other="{{$other}}"
                        ></contact-sitter-form>
                    </div>

                </div>

            </div>
            <div class="col-md-5">
                <calculate-prices ad-data="{{json_encode($ad)}}">
                    <span slot="title">Obračun <small>okvirno *</small></span>
                </calculate-prices>
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <i class="fa fa-info"></i>

                        <h3 class="box-title">Uslovi korišćenja i saveti</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <dl class="">
                            <dt>Plaćanje preko sajta je obavezno</dt>
                            <dd>U obavezi ste da izvršite plaćanje preko sajta. U slučaju da platite na drugi način, izvršićete povredu Pravila korišćenja i vaš nalog će biti suspendovan.</dd>
                            <dt>Hrana</dt>
                            <dd>Ukoliko ostavljate Vašeg ljubimca siteru na duže vreme, obezbedite hranu i sve što je potrebno za njegov ostanak (osim ako niste dogovorili da to siter obezbedi).</dd>

                            <dt>Nadzor</dt>
                            <dd>Siter je dužan da tokom boravka Vašeg ljubimca korsti DogStel app i da snima šetanje Vašeg ljubimca i druge podatke o njegovom boravku.</dd>


                        </dl>
                    </div>
                    <!-- /.box-body -->
                </div>
                {{--<div class="box box-primary direct-chat direct-chat-primary">--}}
                    {{--<div class="box-header with-border">--}}
                        {{--<h3 class="box-title">Direct Chat</h3>--}}

                        {{--<div class="box-tools pull-right">--}}
                            {{--<span data-toggle="tooltip" title="" class="badge bg-light-blue" data-original-title="3 New Messages">3</span>--}}
                            {{--<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>--}}
                            {{--</button>--}}
                            {{--<button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-widget="chat-pane-toggle" data-original-title="Contacts">--}}
                                {{--<i class="fa fa-comments"></i></button>--}}
                            {{--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<!-- /.box-header -->--}}
                    {{--<div class="box-body">--}}
                        {{--<!-- Conversations are loaded here -->--}}
                        {{--<div class="direct-chat-messages">--}}
                            {{--<!-- Message. Default to the left -->--}}
                            {{--<div class="direct-chat-msg">--}}
                                {{--<div class="direct-chat-info clearfix">--}}
                                    {{--<span class="direct-chat-name pull-left">Alexander Pierce</span>--}}
                                    {{--<span class="direct-chat-timestamp pull-right">23 Jan 2:00 pm</span>--}}
                                {{--</div>--}}
                                {{--<!-- /.direct-chat-info -->--}}
                                {{--<img class="direct-chat-img" src="../dist/img/user1-128x128.jpg" alt="Message User Image"><!-- /.direct-chat-img -->--}}
                                {{--<div class="direct-chat-text">--}}
                                    {{--Is this template really for free? That's unbelievable!--}}
                                {{--</div>--}}
                                {{--<!-- /.direct-chat-text -->--}}
                            {{--</div>--}}
                            {{--<!-- /.direct-chat-msg -->--}}

                            {{--<!-- Message to the right -->--}}
                            {{--<div class="direct-chat-msg right">--}}
                                {{--<div class="direct-chat-info clearfix">--}}
                                    {{--<span class="direct-chat-name pull-right">Sarah Bullock</span>--}}
                                    {{--<span class="direct-chat-timestamp pull-left">23 Jan 2:05 pm</span>--}}
                                {{--</div>--}}
                                {{--<!-- /.direct-chat-info -->--}}
                                {{--<img class="direct-chat-img" src="../dist/img/user3-128x128.jpg" alt="Message User Image"><!-- /.direct-chat-img -->--}}
                                {{--<div class="direct-chat-text">--}}
                                    {{--You better believe it!--}}
                                {{--</div>--}}
                                {{--<!-- /.direct-chat-text -->--}}
                            {{--</div>--}}
                            {{--<!-- /.direct-chat-msg -->--}}
                        {{--</div>--}}
                        {{--<!--/.direct-chat-messages-->--}}

                        {{--<!-- Contacts are loaded here -->--}}
                        {{--<div class="direct-chat-contacts">--}}
                            {{--<ul class="contacts-list">--}}
                                {{--<li>--}}
                                    {{--<a href="#">--}}
                                        {{--<img class="contacts-list-img" src="../dist/img/user1-128x128.jpg" alt="User Image">--}}

                                        {{--<div class="contacts-list-info">--}}
                            {{--<span class="contacts-list-name">--}}
                              {{--Count Dracula--}}
                              {{--<small class="contacts-list-date pull-right">2/28/2015</small>--}}
                            {{--</span>--}}
                                            {{--<span class="contacts-list-msg">How have you been? I was...</span>--}}
                                        {{--</div>--}}
                                        {{--<!-- /.contacts-list-info -->--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<!-- End Contact Item -->--}}
                            {{--</ul>--}}
                            {{--<!-- /.contatcts-list -->--}}
                        {{--</div>--}}
                        {{--<!-- /.direct-chat-pane -->--}}
                    {{--</div>--}}
                    {{--<!-- /.box-body -->--}}
                    {{--<div class="box-footer">--}}
                        {{--<form action="#" method="post">--}}
                            {{--<div class="input-group">--}}
                                {{--<input type="text" name="message" placeholder="Type Message ..." class="form-control">--}}
                                {{--<span class="input-group-btn">--}}
                        {{--<button type="submit" class="btn btn-primary btn-flat">Send</button>--}}
                      {{--</span>--}}
                            {{--</div>--}}
                        {{--</form>--}}
                    {{--</div>--}}
                    {{--<!-- /.box-footer-->--}}
                {{--</div>--}}
            </div>
        </div>
@endsection