@extends('front.app')

@section('content')
    <div class="container" id="app" ref="app">
        <div class="row top-margin-30">
            <div class="col-md-4">
                <user-box ad-data="{{json_encode($ad)}}" user-data="{{json_encode($user)}}"></user-box>
                <div class="box services-description">
                    <div class="box-header">

                    </div>
                    <div class="box-body">
                        @foreach($ad->services as $service)
                        <div class="row">
                            <div class="col-md-2 service-icon">
                                <i class="{{$service->icon}}"></i>
                            </div>
                            <div class="col-md-10">
                                <h4>{{$ad->user->firstname}} {{$service->willDo}}</h4>
                                @if($service->pivot->pick_up_drop_off)
                                <li class="text-sm text-uppercase">{{$service->id == 2 ? __('Pickup and drop off included') : __('Pickup and drop off possible')}}</li>
                                @endif
                                @if($service->pivot->holiday_price)
                                    <li class="text-sm text-uppercase">{{__('Will work on holidays')}}</li>
                                @endif
                                @if($service->pivot->additional_dog_price)
                                    <li class="text-sm text-uppercase">{{__('Will give a :percent% discount on 2+ pets', ['percent' => $service->pivot->additional_dog_price])}}</li>
                                @endif

                                @if($service->pivot->more_days_discount)
                                    <li class="text-sm text-uppercase">{{__('Will give a :percent% discount for more than 3 days of service', ['percent' => $service->pivot->more_days_discount])}}</li>
                                @endif
                                <?php $adService = $adServices->where('service_id', $service->id)->first(); ?>


                @if(count($adService->preferences) > 0 )
                                    <hr>
                                <h4>{{__('Preferences')}}</h4>
                @foreach($adService->preferences as $preference)
                    <p><i class="{{$preference->icon}}"></i> {{$preference->title}}</p>
                    @endforeach
                @endif

                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>

                {{--<prices-list services="adData.services"></prices-list>--}}
            </div>
            <div class="col-md-8">

                <display-media ad-data="{{json_encode($ad)}}"></display-media>
                <div class="box about-box">
                    <div class="box-header text-primary">
                        <h3><i class="pe-7s-comment"></i> {{$ad->title}}</h3>
                    </div>
                    <div class="box-content about-me">
                        <p>{!! $ad->about  !!}</p>
                    </div>
                </div>
                <div class="box home-box">
                    <div class="box-header text-primary">
                        <h3><i class="pe-7s-home"></i> {{__('My home')}}</h3>
                    </div>
                    <div class="box-content">
                        <div class="feature">
                            {!!  $ad->residence == "house" ? '<i class="fa fa-home"></i> ' .__('Lives in house') : '<i class="fa fa-building-o"></i> ' . __('Lives in appartment')!!}
                        </div>
                        <div class="feature">
                            <i class="pe-7s-ball"></i> {{$ad->kids == "yes" ? __('Young kids living at home') : __('No young kids living at home')}}
                        </div>
                        @if($ad->car == "yes")
                        <div class="feature">
                            <i class="pe-7s-car"></i> {{$ad->car == "yes" ? __('Have a car for emergency') : __("Don't have a car for emergency")}}
                        </div>
                        @endif
                        @foreach($ad->home_features as $allowance)
                            <div class="feature">
                                <i class="{{$allowance->icon}}"></i> {{$allowance->title}}
                            </div>
                        @endforeach;
                    </div>
                    <hr>
                    <div class="box-content about-me">
                        <p>{!! $ad->about_home  !!}</p>
                    </div>
                </div>
                <div class="box box-rules">
                    <div class="box-header text-primary">
                        <h3><i class="pe-7s-bookmarks"></i> {{__('What is allowed')}}</h3>
                    </div>
                    <div class="box-content">
                        @foreach($ad->allowances as $allowance)
                            <div class="feature">
                                <i class="{{$allowance->icon}}"></i> {{$allowance->title}}
                            </div>
                        @endforeach;
                    </div>

                </div>
                {{--<contact-sitter-form ad-data="{{json_encode($ad)}}" pets="{{json_encode(\Auth::user()->pets)}}"></contact-sitter-form>--}}

            </div>
            {{--<sitter-profile ad="{{json_encode($ad)}}" user="{{json_encode($user)}}"></sitter-profile>--}}
            {{--<contact-sitter-form pets="{{json_encode(\Auth::user()->pets)}}"></contact-sitter-form>--}}
        </div>
@endsection