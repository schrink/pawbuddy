<div class="box box-success">
    <div class="box-header with-border">
        <h3>Podaci o oglasu</h3>
        <h4>Unesite usluge koje želite da pružite i odgovarajuće cene za svaku uslugu koju odaberete.</h4>
    </div>

    <div class="box-body">
        <vf-form action="/ad/set-prices" method="POST" :validation="{
            rules: {
            }}" :options="{
                layout: 'form-horizontal'
            }">
            {{csrf_field()}}
            <vf-status-bar ref="statusbar"></vf-status-bar>
            @foreach($services as $service)
                <service-price-input service-title="{{$service['title']}}" service-id="{{$service['id']}}" value="{{$service['price'] or ""}}"></service-price-input>
            @endforeach
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-danger">
                    <p class="text-center top-margin-20">Na unete cene koje vi želite da zaradite biće obračunata provizija servisa koja iznosi 100,00 RSD po danu po usluzi (bez obzira na vrstu usluge)</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h4 class="text-center">Unesite 3 - 4 fotografije za bolju prezentaciju</h4>
                </div>
            </div>

            @if($images)
            <div class="row">
                <div class="col-md-12">
                    @foreach($images as $image)
                        <image-with-delete url="{{$image->getFullUrl('thumb')}}" image-id="{{$image->id}}"></image-with-delete>
                    @endforeach
                </div>
            </div>
            @endif
            <div class="row bottom-margin-20">
                <div class="col-md-12">

                    <upload-images></upload-images>
                </div>
            </div>

            <vf-select name="sizes" ref="sizes"
                       label="Koje veličine ste spremni da čuvate?"
                       placeholder="Odaberite (moguće je više od jednog izbora)"
                       :items="[
    {id:'Mini', text:'Mini'},
    {id:'Mali', text:'Mali'},
    {id:'Srednji', text:'Srednji'},
    {id:'Veliki', text:'Veliki'},
    {id:'Giant', text:'Giant'}
]
"
                       :multiple="true"
                       :value="{{$ad->sizes ? json_encode($ad->sizes) : json_encode(['Mini', 'Mali', 'Srednji', 'Veliki', 'Giant'])}}"
                       :select2="true"></vf-select>
            <vf-select name="ages" ref="ages"
                   label="Koje uzraste ste spremni da čuvate?"
                       placeholder="Odaberite (moguće je više od jednog izbora)"
                       :items="[
    {id:'Štene', text:'Štene'},
    {id:'Odrastao', text:'Odrastao'},
    {id:'Stariji', text:'Stariji'}
]
"
                       :multiple="true"
                       :value="{{$ad->ages ? json_encode($ad->ages) : json_encode(['Štene', 'Odrastao', 'Stariji'])}}"
                       :select2="true"></vf-select>
            <animal-select dogs="{{$dogs}}" cats="{{$cats}}" other="{{$other}}" multiple="multiple" label="Ako želite da čuvate samo određene rase, odaberite koje" allanimals="" value="{{$ad->breeds_to_care ? json_encode($ad->breeds_to_care): json_encode([])}}"></animal-select>
            <vf-submit text="Snimi"></vf-submit>
        </vf-form>
    </div>
    <!-- /.box-body -->
</div>