<div class="box">
    <div class="box-body">
        <vf-form action="/sitters" method="POST" :validation="{
            rules: {
            }}" :options="{
                layout: 'form-horizontal'
            }">
            {{csrf_field()}}
            <input type="hidden" name="step_finished" value="{{(int)$step}}">

            <vf-status-bar ref="statusbar"></vf-status-bar>
            <vf-text label="Naslov" name="title" ref="title" value="{{$ad->title}}" required></vf-text>
            <vf-textarea label="O vama" name="about" ref="about"
                         placeholder="Predstavite sebe i šta nudite u par reči" :tinymce="false" required
                         value="{{$ad->about}}"></vf-textarea>
            <vf-buttons-list name="residence" ref="residence"
                             label="Tip smeštaja"
                             :items="[{id: 'apartment', text: 'Stan'}, {id: 'house', text: 'Kuća'}]"
                             :value="[{{json_encode($ad->residence)}}]">
            </vf-buttons-list>


            <vf-buttons-list name="kids" ref="kids"
                             label="Da li sa vama žive deca do 10 godina?"
                             :items="[{id: 'yes', text: 'Da'}, {id: 'no', text: 'Ne'}]"
                             :value="[{{json_encode($ad->kids)}}]">
            </vf-buttons-list>

            <conditional-input-with-upload
            label="Ja sam sertifikovani kinolog"
            name="cynologist"
            input-name="cynologist_certificate[]"
            file-name="cynologist_file[]"
            value="{{$ad->cynologist}}"
            media="{{$cynologyCertificates}}"
            media-name="cynology_certificates"

            ></conditional-input-with-upload>

            <conditional-input-with-upload
            label="Ja sam sertifikovani veterinar"
            name="veterinary"
            input-name="veterinary_certificate[]"
            file-name="veterinary_file[]"
            value="{{$ad->veterinary}}"
            media="{{$veterinaryCertificates}}"
            media-name="veterinary_certificates"
            ></conditional-input-with-upload>

            <vf-submit text="Snimi"></vf-submit>
        </vf-form>
    </div>
    <!-- /.box-body -->
</div>