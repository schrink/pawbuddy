<div class="box">
    <div class="box-body">
        <vf-form action="/sitters" method="POST" :validation="{
            rules: {
            }}" :options="{
                layout: 'form-horizontal'
            }">
            {{csrf_field()}}

            <input type="hidden" name="step_finished" value="{{(int)$step}}">
            <input type="hidden" name="ad" value="{{$ad->id}}">

            <services-select services="{{json_encode($services)}}"
                             preselected-services="{{json_encode($selectedServices)}}"
                             preselected-days="{{json_encode($selectedDays)}}"
                             preselected-times="{{json_encode($selectedTimes)}}"
            ></services-select>

            <vf-status-bar ref="statusbar"></vf-status-bar>
            <vf-submit text="Snimi"></vf-submit>
        </vf-form>
    </div>
    <!-- /.box-body -->
</div>