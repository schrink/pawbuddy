<div class="box">
    <div class="box-body">
        <vf-form action="/sitters" method="POST" :validation="{
            rules: {
            }}" :options="{
                layout: 'form-horizontal'
            }">
            {{csrf_field()}}
            <input type="hidden" name="step_finished" value="{{(int)$step}}">
            <input type="hidden" name="ad" value="{{$ad->id}}">

            @foreach($ad->services as $service)

                <div class="service-box">
                    <div class="service-box-left left-border-{{$service->color}}">
                        <i class="{{$service->icon}}"></i>

                    </div>

                    <div class="service-box-right">
                        <h4>{{$service->title}}</h4>
                        <vf-number label="Osnovna cena usluge" name="prices[{{$service->pivot->id}}][price]" ref="prices[{{$service->pivot->id}}][price]" value="{{$service->pivot->price}}"
                                   placeholder="po psu na dan" required></vf-number>
                        <div class="text-right help-block text-black">
                            <small><i class="pe-7s-help1"></i> Osnovna cena usluge za jednog psa po danu usluge. Najniža trenutna cena na sistemu je <strong>{!! Helpers::format_price($minMax[$service->id]['min']) !!}</strong>, a najviša <strong>{!! Helpers::format_price($minMax[$service->id]['max']) !!}</strong>
                            </small>
                        </div>
                        <vf-number label="Cena za nacionalne praznike" name="prices[{{$service->pivot->id}}][holiday_price]" ref="prices[{{$service->pivot->id}}][holiday_price]"
                                   placeholder="po psu na dan" value="{{$service->pivot->holiday_price}}"></vf-number>
                        <div class="text-right help-block text-black">
                            <small><i class="pe-7s-help1"></i> Popunite ovo ukoliko želite da naplaćujete po uvećanoj tarifi kada su praznici
                            </small>
                        </div>
                        <vf-number label="Popust za dodatnog psa (%)" name="prices[{{$service->pivot->id}}][additional_dog_discount]" ref="prices[{{$service->pivot->id}}][additional_dog_discount]"
                                   placeholder="% popusta za više od jednog psa" value="{{$service->pivot->additional_dog_price}}" :rules="{max:80, number: true}"></vf-number>
                        <div class="text-right help-block text-black">
                            <small><i class="pe-7s-help1 "></i> Popunite ovo ukoliko želite da umanjite cenu za slučaj
                                kada je više od jedog psa
                            </small>
                        </div>

                        <vf-number label="Popust na više dana (%)" name="prices[{{$service->pivot->id}}][more_days_discount]" :rules="{max:80, number: true}" ref="prices[{{$service->pivot->id}}][more_days_discount]"
                                   placeholder="% popusta za više od 3 dana" value="{{$service->pivot->more_days_discount}}"></vf-number>
                        <div class="text-right help-block text-black">
                            <small><i class="pe-7s-help1 "></i> Popunite ovo ukoliko želite da umanjite cenu za slučaj
                                kada je više od 3 dana
                            </small>
                        </div>

                        @if(!($service->slug == "walking"))
                            <?php $per = ($service->slug == "sitting" || $service->slug == "walking") ? "po usluzi" : "na dan"?>
                        <vf-number label="Cena za preuzimanje i isporuku" name="prices[{{$service->pivot->id}}][pick_up_drop_off]" ref="prices[{{$service->pivot->id}}][pick_up_drop_off]"
                                   placeholder="računa se jednom {{$per}}" value="{{$service->pivot->pick_up_drop_off}}"></vf-number>
                        <div class="text-right help-block text-black">
                            <small><i class="pe-7s-help1"></i> Ukoliko nudite mogućnost da vi dođete po psa i vratite ga u dogovoreno vreme
                            </small>
                        </div>
                        @endif
                    </div>
                </div>
            @endforeach


            <vf-status-bar ref="statusbar"></vf-status-bar>
            <vf-submit text="Snimi"></vf-submit>
        </vf-form>
    </div>
    <!-- /.box-body -->
</div>