<div class="box">
    <div class="box-body">
        <vf-form action="/sitters" method="POST" :validation="{
            rules: {
            }}" :options="{
                layout: 'form-horizontal'
            }">
            {{csrf_field()}}
            <input type="hidden" name="step_finished" value="{{(int)$step}}">
            <input type="hidden" name="ad" value="{{$ad->id}}">
            <div class="box-header with-border"><h4>Slike</h4></div>
            <div class="row top-margin-10">
                <div class="col-xs-3">
                    <label class="text-right">Uploadujte slike koje najbolje prikazuju Vaše usluge, ambijent u kome će psi provoditi vreme sa Vama i slično</label>

                </div>
                <div class="col-md-9">
                    @if($images)
                        <div class="row">
                            <div class="col-md-12">
                                @foreach($images as $image)
                                    <image-with-delete url="{{$image->getFullUrl('thumb')}}" image-id="{{$image->id}}"></image-with-delete>
                                @endforeach
                            </div>
                        </div>
                    @endif
                    <div class="row bottom-margin-20">
                        <div class="col-md-12">

                            <upload-images></upload-images>
                            <span class="help-block text-right"><i class="pe-7s-help1"></i> Neophodno je uneti makar jednu sliku</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box-header with-border"><h4>O vašem domu</h4></div>
            <div class="top-margin-10"></div>
            <vf-textarea label="Napišite par reči o Vašem domu ukoliko pružate usluge kod Vas" name="about_home" ref="about_home" :tinymce="false" value="{{$ad->about_home}}"></vf-textarea>



            <feature-switch features="{{json_encode($features)}}" feature-values="{{json_encode($selectedFeatures)}}" color="success" resource="home_features"></feature-switch>

            <div class="box-header with-border"><h4>Šta je sve dozvoljeno?</h4></div>
            <div class="top-margin-10"></div>
            <feature-switch features="{{json_encode($allowances)}}" feature-values="{{json_encode($selectedAllowances)}}" color="info" resource="allowances"></feature-switch>


            <vf-status-bar ref="statusbar"></vf-status-bar>
            <vf-submit text="Snimi"></vf-submit>
        </vf-form>
    </div>
    <!-- /.box-body -->
</div>