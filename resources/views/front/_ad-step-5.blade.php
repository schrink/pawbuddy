<div class="box">
    <div class="box-body">
        <vf-form action="/sitters" method="POST" :validation="{
            rules: {
            }}" :options="{
                layout: 'form-horizontal'
            }">
            {{csrf_field()}}
            <input type="hidden" name="step_finished" value="{{(int)$step}}">
            <input type="hidden" name="ad" value="{{$ad->id}}">

            @foreach($ad->services as $service)

                <div class="service-box">
                    <div class="service-box-left left-border-{{$service->color}}">
                        <i class="{{$service->icon}}"></i>

                    </div>

                    <div class="service-box-right">
                        <h4>{{$service->title}}</h4>

                        <vf-number label="Maksimum broj pasa istovremeno" name="services[{{$service->pivot->id}}][maximum_no_of_dogs]"
                                   ref="services[{{$service->pivot->id}}][maximum_no_of_dogs]" value="{{$service->pivot->max_no_of_dogs}}"
                                   placeholder="uneti broj" required></vf-number>
                        <div class="text-right help-block text-black">
                            <small><i class="pe-7s-help1"></i> Koliko pasa najviše od jednom možete da prihvatite za
                                ovu uslugu</strong>
                            </small>

                        </div>
                        <div class="row top-margin-20">
                            <div class="col-xs-3 text-right">
                                <label class="">Veličine pasa (u kg)</label>
                            </div>
                            <div class="col-xs-9">
                                <app-button dt="{{$sizes}}" selected-data="{{json_encode($ad->sizes->pluck('id'))}}" resource="sizes"></app-button>
                            </div>
                        </div>
                        <div class="text-right help-block text-black">
                            <small><i class="pe-7s-help1"></i> Odaberite koje veli;ine pasa prihvatate za ovu uslugu</strong>
                            </small>

                        </div>
                        <div class="row top-margin-20">
                            <div class="col-xs-3 text-right">
                                <label class="">Uzrasti pasa (u god)</label>
                            </div>
                            <div class="col-xs-9">
                                <app-button dt="{{$ages}}" selected-data="{{json_encode($ad->accepting_ages->pluck('id'))}}" resource="accepting_ages"></app-button>
                            </div>
                        </div>
                        <div class="text-right help-block text-black">
                            <small><i class="pe-7s-help1"></i> Odaberite koje uzraste pasa prihvatate za ovu uslugu</strong>
                            </small>

                        </div>
                        <div class="top-margin-20">
                            <?php
                            $sp = (isset($selectedPreferences[$service->pivot->id])) ? $selectedPreferences[$service->pivot->id] : [];
                                ?>
                        <feature-switch features="{{json_encode($preferences)}}"
                                        feature-values="{{json_encode($sp)}}" color="danger"
                                        model="services" ad-id="{{$ad->id}}" service-id="{{$service->id}}"
                                        resource="preferences"></feature-switch>
                        </div>


                    </div>
                </div>
            @endforeach


            <vf-status-bar ref="statusbar"></vf-status-bar>
            <vf-submit text="Snimi"></vf-submit>
        </vf-form>
    </div>
    <!-- /.box-body -->
</div>