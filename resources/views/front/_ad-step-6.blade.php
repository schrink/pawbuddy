<div class="box">
    <div class="box-body">
        <vf-form action="/sitters" method="POST" :validation="{
            rules: {
            }}" :options="{
                layout: 'form-horizontal'
            }">
            {{csrf_field()}}
            <input type="hidden" name="step_finished" value="{{(int)$step}}">
            <input type="hidden" name="ad" value="{{$ad->id}}">

            <div class="box-header with-border"><h4>Informacije za isplatu</h4></div>
            <div class="row text-black top-margin-20">
                <div class="col-xs-9 col-xs-offset-3"><p>Molimo unesite email adresu Vašeg Paypal naloga na koji ćemo Vam transferovati Vašu zaradu. Novac se transferuje odmah i stiže Vam onog momenta kada PayPal procesira plaćanje. Po unosu mejla stići će Vam na tu adresu email za verifikaciju. Uplate neće biti isplaćivane dok ne potvrdite email adresu.</p></div>
            </div>

            <vf-email label="Paypal email" name="paypal_email" ref="paypal_email" value="{{$ad->paypal_email}}" required></vf-email>

            <vf-text label="Ime na nalogu" name="paypal_name" ref="paypal_name" value="{{$ad->paypal_name}}" ></vf-text>

            <vf-text label="Prezime na nalogu" name="paypal_lastname" ref="paypal_lastname" value="{{$ad->paypal_lastname}}" ></vf-text>

            <div class="box-header with-border"><h4>Aktivacija</h4></div>
            <div class="row text-black top-margin-20">
                <div class="col-xs-9 col-xs-offset-3"><p>Čestitamo! Završili ste prijavu za dog sitera. Kao poslednji korak, Vaše informacije će biti proverene od strane našeg agenta putem telefona ili digitalno. Do tada, vaš nalog je u statusu "na čekanju". </p></div>
            </div>
            <div class="row text-black top-margin-20">
                <div class="col-xs-9 col-xs-offset-3"><p>Molimo Vas da pažljivo pročitate naše uslove servisa. </p></div>
            </div>

            <vf-checkbox label="Saglasan sam sa uslovima korišćenja servise" name="tos" ref="tos" value="yes" required></vf-checkbox>

            <vf-status-bar ref="statusbar"></vf-status-bar>
            <vf-submit text="Snimi"></vf-submit>
        </vf-form>
    </div>
    <!-- /.box-body -->
</div>