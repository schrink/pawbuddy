<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse pull-left" id="navbar-collapse">
    <ul class="nav navbar-nav">
        <li {{Helpers::setActive('sitters')}}>
            <a href="{{route('sitters')}}">Pronađi sitera</a></li>
        <li {{Helpers::setActive('ad/create')}}>
            @if($user->role_id == 2 )
            <a href="{{route('sitters.register')}}">Postani siter</a></li>
            @elseif($user->role_id == 5)
            <a href="{{route('sitters.register')}}">Registracija sittera <span class="badge bg-aqua">započeto</span></a></li>
            @else
            <a href="{{route('sitters.register')}}">Podešavanja sittera</a></li>
            @endif

            {{--<li class="dropdown">--}}
        {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <span class="caret"></span></a>--}}
        {{--<ul class="dropdown-menu" role="menu">--}}
        {{--<li><a href="#">Action</a></li>--}}
        {{--<li><a href="#">Another action</a></li>--}}
        {{--<li><a href="#">Something else here</a></li>--}}
        {{--<li class="divider"></li>--}}
        {{--<li><a href="#">Separated link</a></li>--}}
        {{--<li class="divider"></li>--}}
        {{--<li><a href="#">One more separated link</a></li>--}}
        {{--</ul>--}}
        {{--</li>--}}
    </ul>
    <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
            <input type="text" class="form-control" id="navbar-search-input" placeholder="Search">
        </div>
    </form>
</div>
<!-- /.navbar-collapse -->
<!-- Navbar Right Menu -->
<div class="navbar-custom-menu">
    <ul class="nav navbar-nav">
        <!-- Messages: style can be found in dropdown.less-->
        <li class="dropdown messages-menu">
            <!-- Menu toggle button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-envelope-o"></i>
                <span class="label label-success">4</span>
            </a>
            <ul class="dropdown-menu">
                <li class="header">You have 4 messages</li>
                <li>
                    <!-- inner menu: contains the messages -->
                    <ul class="menu">
                        <li><!-- start message -->
                            <a href="#">
                                <div class="pull-left">
                                    <!-- User Image -->
                                    {{--<img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">--}}
                                </div>
                                <!-- Message title and timestamp -->
                                <h4>
                                    Support Team
                                    <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                </h4>
                                <!-- The message -->
                                <p>Why not buy a new awesome theme?</p>
                            </a>
                        </li>
                        <!-- end message -->
                    </ul>
                    <!-- /.menu -->
                </li>
                <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
        </li>
        <!-- /.messages-menu -->

        <!-- Notifications Menu -->
        <li class="dropdown notifications-menu">
            <!-- Menu toggle button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bell-o"></i>
                @if($user->ad->unreadNotifications->count() + $user->unreadNotifications->count() > 0)
                <span class="label label-warning">{{$user->ad->notifications->count() + $user->notifications->count()}}</span>
                @endif
            </a>
            <ul class="dropdown-menu">
                <li class="header">Imate {{$user->ad->unreadNotifications->count() + $user->unreadNotifications->count()}} novih notifikacija</li>
                <li>
                    <!-- Inner Menu: contains the notifications -->
                    <ul class="menu">
                        @foreach($user->notifications() as $notification)
                            <li><!-- start notification -->
                                <a href="#">
                                    <i class="{{$notification->data['class']}}"></i> {{$notification->data['message']}}
                                </a>
                            </li>
                        @endforeach
                        @foreach($user->ad->notifications as $notification)
                            <li><!-- start notification -->
                                <a href="#">
                                    <i class="{{$notification->data['class']}}"></i> {{$notification->data['message']}}
                                </a>
                            </li>
                    @endforeach

                    <!-- end notification -->
                    </ul>
                </li>
                <li class="footer"><a href="#">View all</a></li>
            </ul>
        </li>

        @isset($user)
            <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <!-- The user image in the navbar-->
                    <img src="{{ $user->getFirstMediaUrl('avatar', 'thumb') ? $user->getFirstMediaUrl('avatar', 'thumb') : \Avatar::create($user->name)->toBase64() }}"
                         class="user-image" alt="User Image">
                    <!-- hidden-xs hides the username on small devices so only the image appears. -->
                    <span class="hidden-xs">{{ $user->name }}</span>
                </a>
                <ul class="dropdown-menu">
                    <!-- The user image in the menu -->
                    <li class="user-header">
                        <img src="{{ Helpers::getProfileImage() }}" class="img-circle" alt="User Image">

                        <p>
                            {{ $user->name }}
                            <small>Member since {{ $user->created_at->format('F Y.') }}</small>
                        </p>
                    </li>
                    <!-- Menu Body -->
                    <li class="user-body">
                        <div class="row">
                            <div class="col-xs-4 text-center">
                                <a href="#">Followers</a>
                            </div>
                            <div class="col-xs-4 text-center">
                                <a href="#">Sales</a>
                            </div>
                            <div class="col-xs-4 text-center">
                                <a href="#">Friends</a>
                            </div>
                        </div>
                        <!-- /.row -->
                    </li>
                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="pull-left">
                            <a href="/me" class="btn btn-default btn-flat">Profile</a>
                        </div>
                        <div class="pull-right">
                            <a href="{!! url('/logout') !!}" class="btn btn-default btn-flat"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                Sign out
                            </a>
                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </li>
                </ul>
            </li>
        @endisset
    </ul>
</div>
<!-- /.navbar-custom-menu -->