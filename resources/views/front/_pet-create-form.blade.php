<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Dodaj ljubimca</h3>
    </div>
    <div class="box-body">

        <pet-create-form animal-types="{{$animalTypes}}" dogs="{{$dogs}}" cats="{{$cats}}"
                         other="{{$other}}"></pet-create-form>


        {{--<vf-form action="/pet/create" method="POST" :validation="{--}}
            {{--rules: {--}}
            {{--}}" :options="{--}}
                {{--layout: 'form-horizontal'--}}
            {{--}">--}}
            {{--{{csrf_field()}}--}}
            {{--<vf-status-bar ref="statusbar"></vf-status-bar>--}}
            {{--<vf-text label="Ime" name="name" ref="name" value="" required></vf-text>--}}
            {{--<vf-file label="Slika" placeholder="Dodaj sliku svog ljubimca" ref="image" name="image"></vf-file>--}}
            {{--<animal-select dogs="{{$dogs}}" cats="{{$cats}}" other="{{$other}}" label="Vrsta" :allanimals="true" value="{{json_encode([])}}"></animal-select>--}}

            {{--<vf-buttons-list name="gender" ref="gender"--}}
                             {{--label="Pol"--}}
                             {{--:items="[{id: 1, text: 'Ženski'}, {id: 2, text: 'Muški'}]">--}}
            {{--</vf-buttons-list>--}}
            {{--<vf-date label="Datum rođenja" name="birthday" ref="birthday" placeholder="Odaberi datum"--}}
                     {{--format="DD. MMMM YYYY." required></vf-date>--}}

            {{--<vf-buttons-list name="social" ref="social"--}}
                             {{--label="Da li je druželjubiv?"--}}
                             {{--:items="[{id: 1, text: 'Da'}, {id: 2, text: 'Ne'}]">--}}
            {{--</vf-buttons-list>--}}
            {{--<vf-buttons-list name="sterilised" ref="sterilised"--}}
                             {{--label="Da li je sterilisan?"--}}
                             {{--:items="[{id: 1, text: 'Da'}, {id: 2, text: 'Ne'}]">--}}
            {{--</vf-buttons-list>--}}

            {{--<vf-buttons-list name="vaccinated" ref="vaccinated"--}}
                             {{--label="Da li ima sve neophodne vakcine?"--}}
                             {{--:items="[{id: 1, text: 'Da'}, {id: 2, text: 'Ne'}]">--}}
            {{--</vf-buttons-list>--}}

            {{--<vf-buttons-list name="healthy" ref="healthy"--}}
                             {{--label="Da li je vaš ljubimac zdrav?"--}}
                             {{--:items="[{id: 1, text: 'Da'}, {id: 2, text: 'Ne'}]">--}}
            {{--</vf-buttons-list>--}}
            {{--<vf-textarea label="Ukoliko nije zdrav, objasnite" name="healthy_description" ref="healthy_description"--}}
                         {{--placeholder="Objasnjenje u dve rečenice"></vf-textarea>--}}
            {{--<vf-submit></vf-submit>--}}
        {{--</vf-form>--}}
    </div>
    <!-- /.box-body -->
</div>