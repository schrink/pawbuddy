<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title display-block padding-30"><i class="fa fa-home margin-r-5"></i> <b>{{$ad->services[0]->title}} <small>na dan</small></b> <span class="pull-right"><strong>{!! Helpers::format_price($ad->services[0]->pivot->price) !!}</strong></span></h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">

    @if($ad->services[0]->pivot->holiday_price)
            <b>Cena za praznike</b> <span class="pull-right"><strong>{!! Helpers::format_price($ad->services[0]->pivot->holiday_price) !!}</strong></span>
            <hr>
    @endif

        @if($ad->services[0]->pivot->additional_dog_price)
        <b>Cena za dodatnog psa</b> <span class="pull-right"><strong>{!! Helpers::format_price($ad->services[0]->pivot->additional_dog_price) !!}</strong></span>
        <hr>
        @endif

        @if($ad->services[0]->pivot->pick_up_drop_off)
        <b>Dolazak i vraćanje</b> <span class="pull-right"><strong>{!! Helpers::format_price($ad->services[0]->pivot->pick_up_drop_off) !!}</strong></span>
        <hr>
        @endif
        @if($ad->services[0]->pivot->more_days_discount)
        <b>Popust na preko 3 dana</b> <span class="pull-right"><strong>{{$ad->services[0]->pivot->more_days_discount}}%</strong></span>
        @endif

    </div>
    <!-- /.box-body -->
</div>
