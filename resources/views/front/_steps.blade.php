<div class="row bs-wizard" style="border-bottom:0;">

    <div class="col-xs-2 bs-wizard-step {{ ($step == 1) ? "active" : ""}} {{$step > 1  || $ad->step_finished >= 1 ? "complete" : ""}} {{$step < 1  && $ad->step_finished < 1 ? "disabled" : ""}}">
        <div class="text-center bs-wizard-stepnum">Informacije o vama</div>
        <div class="progress">
            <div class="progress-bar"></div>
        </div>
        <a href="{{route('sitters.register', 1)}}" class="bs-wizard-dot"></a>
        <div class="bs-wizard-info text-center"> Osnovne informacije o Vama i uslugama koje nudite.</div>
    </div>

    <div class="col-xs-2 bs-wizard-step {{ ($step == 2) ? "active" : ""}} {{$step > 2  || $ad->step_finished >= 2 ? "complete" : ""}} {{$step < 2  && $ad->step_finished < 2 ? "disabled" : ""}}"><!-- complete -->
        <div class="text-center bs-wizard-stepnum">Usluge i raspored</div>
        <div class="progress">
            <div class="progress-bar"></div>
        </div>
        <a href="{{route('sitters.register', 2)}}" class="bs-wizard-dot"></a>
        <div class="bs-wizard-info text-center"> Odabir usluga koje želite da pružite, raspored dana i termina kada radite.
        </div>
    </div>

    <div class="col-xs-2 bs-wizard-step {{ ($step == 3) ? "active" : ""}} {{$step > 3 || $ad->step_finished >= 3 ? "complete" : ""}} {{$step < 3 && $ad->step_finished < 3 ? "disabled" : ""}}"><!-- complete -->
        <div class="text-center bs-wizard-stepnum">Cene</div>
        <div class="progress">
            <div class="progress-bar"></div>
        </div>
        <a href="{{route('sitters.register', 3)}}" class="bs-wizard-dot"></a>
        <div class="bs-wizard-info text-center"> Cene svh usluga koje nudite, popusti i dodatne tarife za posebne usluge.
        </div>
    </div>

    <div class="col-xs-2 bs-wizard-step {{ ($step == 4) ? "active" : ""}} {{$step > 4  || $ad->step_finished >= 4 ? "complete" : ""}} {{$step < 4  && $ad->step_finished < 4 ? "disabled" : ""}}"><!-- active -->
        <div class="text-center bs-wizard-stepnum">Opis usluga</div>
        <div class="progress">
            <div class="progress-bar"></div>
        </div>
        <a href="{{route('sitters.register', 4)}}" class="bs-wizard-dot"></a>
        <div class="bs-wizard-info text-center"> Slike i opis Vaših usluga, šta je sve dozvoljeno i šta od pogodnosti nudite.
        </div>
    </div>
    <div class="col-xs-2 bs-wizard-step {{ ($step == 5) ? "active" : ""}} {{$step > 5  || $ad->step_finished >= 5 ? "complete" : ""}} {{$step < 5  && $ad->step_finished < 5 ? "disabled" : ""}}"><!-- active -->
        <div class="text-center bs-wizard-stepnum">Preference</div>
        <div class="progress">
            <div class="progress-bar"></div>
        </div>
        <a href="{{route('sitters.register', 5)}}" class="bs-wizard-dot"></a>
        <div class="bs-wizard-info text-center"> Vaše preference i ograničenja.
        </div>
    </div>
    <div class="col-xs-2 bs-wizard-step {{ ($step == 6) ? "active" : ""}} {{$step > 6  || $ad->step_finished >= 6 ? "complete" : ""}} {{$step < 6  && $ad->step_finished < 6 ? "disabled" : ""}}"><!-- active -->
        <div class="text-center bs-wizard-stepnum">Aktivacija</div>
        <div class="progress">
            <div class="progress-bar"></div>
        </div>
        <a href="{{route('sitters.register', 6)}}" class="bs-wizard-dot"></a>
        <div class="bs-wizard-info text-center"> Aktivacija Vašeg oglasa i provera informacija.
        </div>
    </div>
</div>
