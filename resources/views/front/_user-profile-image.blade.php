<div class="box box-primary">
    <div class="box-body box-profile">
            <img class="profile-user-img img-responsive img-circle profile-square" src="{{ Helpers::getProfileImage($user) }}" alt="{{ $user->name }}">

        <h3 class="profile-username text-center">{{ $user->name }}</h3>
        @if($ad->published)
        <p class="text-muted text-center"><span class="label label-danger">Pet Sitter</span></p>
        @endif
        <p class="text-muted text-center">{{$user->city}}</p>
        <p class="text-center"><star-rating :rating="{{$ad->published ? $ad->rating : $user->rating}}" :read-only="true" :increment="0.01" :star-size="20" :inline="true" :show-rating="false"></star-rating></p>

        @if($user == \Auth::user())
        <a href="/profile/edit" class="btn btn-primary btn-block"><b>Edit</b></a>
        @endif
    </div>
    <!-- /.box-body -->
</div>