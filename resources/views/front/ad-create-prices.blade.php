@extends('front.app')

@section('content')
    <div class="container" id="app">
        <div class="row top-margin-30">
            <div class="col-md-4">
                @include("front._user-profile-image")
            </div>
            <div class="col-md-8">
                @include('front._ad-create-prices-form')
            </div>
        </div>
    </div>
@endsection