@extends('front.app')

@section('content')
    <div class="container" id="app" ref="app">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success top-margin-40">
                    <div class="box-header with-border">
                        <h4>Registracija za sitera</h4>
                    </div>
                    <div class="box-body">
                        @include('front._steps')
                    </div>
                </div>
                @include('front._ad-step-' . $step)
            </div>

        </div>
    </div>
@endsection