@extends('front.app')

@section('content')
    <div class="" id="app" style="display: block;" ref="app">
        <div class="row top-margin-30">
            <div class="col-md-12">
                @include('front._search-form')
            </div>
        </div>
        <div class="row">
            {{--<div class="col-md-3">--}}
            {{--@include("front._user-profile-image")--}}
            {{--</div>--}}
            <div class="col-md-7 ">

                <ads-display ads="{{json_encode($ads)}}"></ads-display>

            </div>
            <div class="col-md-5">
                <div class="cf">
                <gmap lat-lng="{{json_encode(\Auth::user()->lat_lng)}}" users="{{json_encode($ads->toArray())}}" v-cloak></gmap>
                </div>
            </div>
        </div>
    </div>
@endsection