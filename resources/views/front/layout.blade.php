<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <link rel="shortcut icon" type="image/png" href="favicon.png">

    <link rel="stylesheet" href="{{mix('/css/app.css')}}">
    <link rel="stylesheet" type="text/css" href="{{mix('/css/front.css')}}">
    <link rel="stylesheet" href="./css/animate.min.css"><link rel="stylesheet" href="./css/font-awesome.min.css"><link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,800,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <link href='https://fonts.googleapis.com/css?family=Josefin+Sans&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

    <script src="./js/jquery-2.1.0.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/blocs.min.js"></script>
    <script src="./js/jqBootstrapValidation.js"></script>
    <script src="./js/formHandler.js"></script>
    <title>Home</title>


    <!-- Google Analytics -->

    <!-- Google Analytics END -->

</head>
<body>
<!-- Main container -->
<div class="page-container">

    <!-- bloc-0 -->
    <div class="bloc sticky-nav bgc-white l-bloc " id="bloc-0">
        <div class="container">
            <nav class="navbar row">
                <div class="navbar-header">
                    <a class="navbar-brand" href="/"><img src="/img/logo-paw.png" width="140" alt=""></a>
                    <button id="nav-toggle" type="button" class="ui-navbar-toggle navbar-toggle" data-toggle="collapse" data-target=".navbar-1">
                        <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse navbar-1">
                    <ul class="site-navigation nav navbar-nav">
                        <li>
                            <a href="/login">Logovanje</a>
                        </li>
                        <li>
                            <a href="/register">Registracija</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
    <!-- bloc-0 END -->

    <!-- bloc-1 -->
    <div class="bloc bloc-fill-screen bg-pexels-photo-115025 d-bloc bgc-1" id="bloc-1">
        <div class="video-bg-container">
            {{--<video class="bloc-video" preload="auto" loop="loop" autoplay="autoplay">--}}
                {{--<source src="/vid/647910908.mp4" type="video/mp4" />--}}
            {{--</video>--}}

        </div>
        <div class="container fill-bloc-top-edge">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="mg-md  lg-shadow tc-white text-center">Čuvanje Vašeg ljubimca</h1>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row animated puffIn">
                <div class="col-sm-6">
                    <h1 class="mg-md  lg-shadow tc-white">
                        Dok ste na putu?
                    </h1>
                    <form id="form_12826" novalidate>
                        <div class="form-group">
                            <label>
                                Datum
                            </label>
                            <input class="form-control" type="date" id="input_1006" />
                        </div><a href="index.html" class="btn btn-block btn-boston-university-red btn-lg">Čuvanje</a>
                    </form>
                </div>
                <div class="col-sm-6">
                    <h1 class="mountaindusk-hero-text fadeIn  tc-white animated">
                        Dok ste na poslu?
                    </h1>
                    <form id="form_8684" novalidate>
                        <div class="form-group">
                            <label>
                                Datum
                            </label>
                            <input class="form-control" type="date" id="input_1550" />
                        </div>
                        <div class="text-center">
                            <a href="index.html" class="btn btn-block btn-lg btn-cinereous">Vrtić</a>
                        </div>
                        <div class="text-center">
                            <a href="index.html" class="btn btn-block btn-orange-peel btn-lg">Šetnja</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="container fill-bloc-bottom-edge">
            <div class="row">
                <div class="col-sm-12">
                    <a id="scroll-hero" class="blocs-hero-btn-dwn" href="#"><span class="fa fa-chevron-down"></span></a>
                </div>
            </div>
        </div>
    </div>
    <!-- bloc-1 END -->

    <!-- ScrollToTop Button -->
    <a class="bloc-button btn btn-d scrollToTop" onclick="scrollToTarget('1')"><span class="fa fa-chevron-up"></span></a>
    <!-- ScrollToTop Button END-->


    <!-- Footer - bloc-2 -->
    <div class="bloc bgc-medium-jungle-green d-bloc" id="bloc-2">
        <div class="container bloc-md">
            <div class="row row-no-gutters">
                <div class="col-xs-12 col-md-8 col-md-offset-2">
                    <h2 class="statement-bloc-text  vanishIn animated">
                        Odlazite a nemate kome da ostavite ljubimca?
                    </h2>
                    <h2 class="mg-md text-center  sm-shadow vanishIn animDelay02 animated">
                        <i>Ništa ne brinite. Naši petsiteri će ga paziti dok vas nema!</i>
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer - bloc-2 END -->

    <!-- Footer - bloc-3 -->
    <div class="bloc b-parallax bg-pexels-photo-10517 d-bloc bgc-1" id="bloc-3">
        <div class="container bloc-lg">
            <div class="row padding-30">
                <div class="col-sm-4">
                    <div class="text-center">
                        <span class="fa fa-heart icon-round icon-md"></span>
                    </div>
                    <h2 class="text-center mg-md">
                        Reci ne boksovima!
                    </h2>
                    <p class="text-center">
                        A little feature description could go here.A little feature description could go here.
                    </p>
                </div>
                <div class="col-sm-4">
                    <div class="text-center">
                        <span class="fa fa-rocket icon-round icon-md"></span>
                    </div>
                    <h2 class="text-center mg-md">
                        Individualni pristup
                    </h2>
                    <p class="text-center">
                        A little feature description could go here. A little feature description could go here.
                    </p>
                </div>
                <div class="col-sm-4">
                    <div class="text-center">
                        <span class="fa fa-code icon-round icon-md"></span>
                    </div>
                    <h2 class="text-center mg-md">
                        Na vezi 24/7
                    </h2>
                    <p class="text-center">
                        A little feature description could go here.A little feature description could go here.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer - bloc-3 END -->

    <!-- Footer - bloc-4 -->
    <div class="bloc bgc-white l-bloc" id="bloc-4">
        <div class="container bloc-lg">
            <div class="row">
                <div class="col-sm-6">
                    <img src="img/placeholder-image.png" class="img-responsive" />
                </div>
                <div class="col-sm-6">
                    <h2 class="mg-lg">
                        Čuvanje u kućnim uslovima
                    </h2>
                    <h3 class="mg-sm">
                        Bolje od pansiona
                    </h3>
                    <p class="mg-lg">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
                    </p><a href="index.html" class="btn btn-d btn-lg">Pročitaj više</a>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer - bloc-4 END -->

    <!-- Footer - bloc-5 -->
    <div class="bloc b-parallax bgc-1 bg-wall-animal-dog-pet d-bloc" id="bloc-5">
        <div class="container bloc-lg">
            <div class="row">
                <div class="col-sm-10">
                    <h3 class="mg-md blocs-mobile-signup-text text-left lg-shadow">
                        Prijavi se da postaneš petsitter
                    </h3>
                </div>
                <div class="col-sm-2">
                    <div class="text-center">
                        <a href="index.html" class="btn btn-lg btn-wire">Registracija</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer - bloc-5 END -->

    <!-- Footer - bloc-6 -->
    <div class="bloc bgc-white l-bloc" id="bloc-6">
        <div class="container bloc-lg">
            <div class="row">
                <div class="col-sm-3">
                    <img src="img/placeholder-image.png" class="img-responsive" />
                    <h3 class="mg-md">
                        Provera petsittera
                    </h3>
                    <p>
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
                    </p>
                </div>
                <div class="col-sm-3">
                    <img src="img/placeholder-image.png" class="img-responsive" />
                    <h3 class="mg-md">
                        Redovne provere&nbsp;
                    </h3>
                    <p>
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
                    </p>
                </div>
                <div class="col-sm-3">
                    <img src="img/placeholder-image.png" class="img-responsive" />
                    <h3 class="mg-md">
                        Pravna zaštita
                    </h3>
                    <p>
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
                    </p>
                </div>
                <div class="col-sm-3">
                    <img src="img/placeholder-image.png" class="img-responsive" />
                    <h3 class="mg-md">
                        Na vezi 24/7
                    </h3>
                    <p>
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer - bloc-6 END -->

    <!-- Footer - bloc-7 -->
    <div class="bloc bgc-medium-jungle-green d-bloc" id="bloc-7">
        <div class="container bloc-lg">
            <div class="row">
                <div class="col-sm-3">
                    <h3 class="mg-md bloc-mob-center-text">
                        About
                    </h3><a href="index.html" class="a-btn a-block bloc-mob-center-text">The team</a><a href="index.html" class="a-btn a-block bloc-mob-center-text">Contact us</a>
                </div>
                <div class="col-sm-3">
                    <h3 class="mg-md bloc-mob-center-text">
                        Get the App
                    </h3><a href="index.html" class="a-btn a-block bloc-mob-center-text">iPhone</a><a href="index.html" class="a-btn a-block bloc-mob-center-text">Android</a>
                </div>
                <div class="col-sm-3">
                    <h3 class="mg-md bloc-mob-center-text">
                        Follow Us
                    </h3><a href="index.html" class="a-btn a-block bloc-mob-center-text">Twitter</a><a href="index.html" class="a-btn a-block bloc-mob-center-text">Facebook</a>
                </div>
                <div class="col-sm-3">
                    <h3 class="mg-md bloc-mob-center-text">
                        Company
                    </h3><a href="index.html" class="a-btn a-block bloc-mob-center-text">Terms of use</a><a href="index.html" class="a-btn a-block bloc-mob-center-text">Privacy</a>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer - bloc-7 END -->

</div>
<!-- Main container END -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
@include('sweet::alert')
</body>
</html>
