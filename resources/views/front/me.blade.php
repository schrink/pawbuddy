@extends('front.app')

@section('content')
    <div class="container" id="app" ref="app">
        <my-profile ad="{{json_encode($ad)}}" user="{{json_encode($user)}}"></my-profile>
    </div>
@endsection