@extends('front.app')

@section('content')
    <div class="container" id="app" ref="app">
        <div class="row top-margin-30">
            <div class="col-md-4">
                {{--@include("front._user-profile-image")--}}
            </div>
            <div class="col-md-8">
                @include('front._pet-create-form')
            </div>
        </div>
    </div>
@endsection