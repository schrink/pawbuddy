{{--<gmap lat-lng="{{json_encode($user->lat_lng)}}"></gmap>--}}
<vf-form action="process-form" method="POST" :validation="{
            rules: {
            }}" :options="{
                layout: 'form-horizontal'
            }">
    {{csrf_field()}}
    <vf-status-bar ref="statusbar"></vf-status-bar>
    <vf-text label="Ime i prezime" name="name" ref="name" value="{{$user->name}}" required></vf-text>
    <vf-email label="Email" name="email" ref="email" required value="{{$user->email}}"></vf-email>
    <vf-text label="Grad" name="city" ref="city" value="{{$user->city}}" required></vf-text>
    <vf-text label="Država" name="country" ref="country" value="{{$user->country}}" required></vf-text>
    <vf-buttons-list name="gender" ref="gender"
                     label="Pol"
                     :items="[{id: 'female', text: 'Ženski'}, {id: 'male', text: 'Muški'}]"
                     :value="{{json_encode($user->gender)}}"
    >
    </vf-buttons-list>
    <div class="form-group">
        <div class="col-sm-3 text-right">
            <label for="birthday">Dan rođenja</label>
        </div>
        <div class="col-sm-9">
        <date-picker value="{{$user->birthdate}}" input-class="form-control" :monday-first="true" name="birthdate" :required="true"></date-picker>
        </div>
    </div>

    <hr>
    <div class="box-header">
        <h4 class="text-center text-info">Dalje informacije neće biti javno dustupne na sajtu i koristiće se samo u slučaju da imamo potrebu da Vas hitno kontaktiramo</h4>
        <p class="text-sm text-center">Vaše informacije su sigurne i neće biti davane na uvid trećim licima. Za sve nedoumice možete pogledati našu <a
                    href="/privacy-policy" target="_blank">Politiku privatnosti</a></p>
    </div>
    <vf-text label="Kontakt telefon" name="phone" ref="phone" value="{{$user->phone}}" required></vf-text>

    <vf-text label="Adresa" name="address" ref="address" value="{{$user->address}}" required></vf-text>

    <vf-text label="Kontakt telefon za hitne slučajeve" name="emergency_phone" ref="emergency_phone" value="{{$user->emergency_phone}}" required></vf-text>
    <vf-text label="Ime osobe za hitne slučajeve" name="emergency_name" ref="emergency_name" value="{{$user->emergency_name}}" required></vf-text>
    {{--<vf-text label="JMBG" :rules="{ max: 13, min: 13, digits: true, required: true}" name="personal_id" value="{{$user->personal_id}}" ref="jmbg"></vf-text>--}}
    <vf-submit></vf-submit>
</vf-form>