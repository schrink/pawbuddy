<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Breed Field -->
<div class="form-group col-sm-6">
    {!! Form::label('breed', 'Breed:') !!}
    {!! Form::text('breed', null, ['class' => 'form-control']) !!}
</div>

<!-- Age Field -->
<div class="form-group col-sm-6">
    {!! Form::label('age', 'Age:') !!}
    {!! Form::text('age', null, ['class' => 'form-control']) !!}
</div>

<!-- Gender Field -->
<div class="form-group col-sm-12">
    {!! Form::label('gender', 'Gender:') !!}

</div>

<!-- Social Field -->
<div class="form-group col-sm-12">
    {!! Form::label('social', 'Social:') !!}

</div>

<!-- Vaccinated Field -->
<div class="form-group col-sm-12">
    {!! Form::label('vaccinated', 'Vaccinated:') !!}

</div>

<!-- Healthy Field -->
<div class="form-group col-sm-12">
    {!! Form::label('healthy', 'Healthy:') !!}

</div>

<!-- Illness Field -->
<div class="form-group col-sm-6">
    {!! Form::label('illness', 'Illness:') !!}
    {!! Form::text('illness', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('pets.index') !!}" class="btn btn-default">Cancel</a>
</div>
