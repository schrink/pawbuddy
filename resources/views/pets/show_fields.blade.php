<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $pet->id !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $pet->user_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $pet->name !!}</p>
</div>

<!-- Breed Field -->
<div class="form-group">
    {!! Form::label('breed', 'Breed:') !!}
    <p>{!! $pet->breed !!}</p>
</div>

<!-- Age Field -->
<div class="form-group">
    {!! Form::label('age', 'Age:') !!}
    <p>{!! $pet->age !!}</p>
</div>

<!-- Gender Field -->
<div class="form-group">
    {!! Form::label('gender', 'Gender:') !!}
    <p>{!! $pet->gender !!}</p>
</div>

<!-- Social Field -->
<div class="form-group">
    {!! Form::label('social', 'Social:') !!}
    <p>{!! $pet->social !!}</p>
</div>

<!-- Vaccinated Field -->
<div class="form-group">
    {!! Form::label('vaccinated', 'Vaccinated:') !!}
    <p>{!! $pet->vaccinated !!}</p>
</div>

<!-- Healthy Field -->
<div class="form-group">
    {!! Form::label('healthy', 'Healthy:') !!}
    <p>{!! $pet->healthy !!}</p>
</div>

<!-- Illness Field -->
<div class="form-group">
    {!! Form::label('illness', 'Illness:') !!}
    <p>{!! $pet->illness !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $pet->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $pet->updated_at !!}</p>
</div>

