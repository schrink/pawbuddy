@extends('front.app')

@section('content')
    <div class="container" id="app">
        <div class="row top-margin-30">
            <div class="col-md-4">
                <user-box ad-data="{{json_encode($user->ad)}}" user-data="{{json_encode($user)}}"></user-box>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Moji Ljubimci</h3>
                    </div>
                    <div class="box-body">
                        <a href="/pet/create" class="btn btn-primary btn-block"> <i class="pe-7s-plus"></i> Dodaj ljubimca</a>

                    </div>
                </div>
            </div>

            <div class="col-md-8">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3>Moj Profil</h3>
                    </div>
                    <div class="box-body">
                        @include('partials/profile-form')
                    </div>
                </div>
            </div>


        </div>
    </div>
@endsection