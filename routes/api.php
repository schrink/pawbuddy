<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('breeds', function (Request $request) {
    $breeds = \App\Breed::all();
    foreach ($breeds as $key => $breed) {
        $br[$key]['id'] = $breed->id;
        $br[$key]['text'] = $breed->breedname;
    }
    return $br;
});

Route::post('upload/certificates', 'FileController@uploadCertificate')->middleware("auth:api");

Route::post('files/upload-images', 'FileController@uploadAdImages')->middleware("auth:api");
Route::get('get-certificates', 'FileController@getCertificatesApi')->middleware("auth:api");
Route::get('download-certificate', 'FileController@downloadCertificateApi')->middleware("auth:api");
Route::post('delete-file', 'FileController@deleteMediaApi')->middleware("auth:api");
Route::post('pet', 'PetController@petSave')->middleware('auth:api');
Route::group(['middleware' => ['auth:api'], 'prefix' => 'sitters'], function (){
    /**
     * @api {get} /sitters Index of all Sitters
     * @apiDescription You can apply filters (TODO: pagination)
     * @apiName GetSitters
     * @apiGroup Sitters
     * @apiVersion 1.0.0
     *
     * @apiParam {json} [fiters] Object containing all the filters
     * @apiParam {int} [fiters.service] Service ID - 1 (sitting), 2 (walking), 3 (daily)
     * @apiParam {array} [fiters.price] Array containing minimum and maximum price i.e [100, 20000]
     * @apiParam {int} fiters.price.min Minimum price
     * @apiParam {int} fiters.price.max Maximum price
     * @apiParam {array} [fiters.sizes] Array of sizes IDs (1, 2, 3, 4, 5)
     * @apiParam {array} [fiters.times] Array of times IDs (1, 2, 3)
     * @apiParam {array} [fiters.days] Array of days ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun']
     * @apiParam {int} [fiters.noOfDogs] Maximum no of dogs
     *
     * @apiParamExample {json} Request-Example:
     *     {
     *       "filters": {
     *            'service' : 1,
     *            'price': [100, 1000],
     *            'sizes': [1,4,5],
     *            'times': [1, 2],
     *            'days': ['mon', 'fri'],
     *            'noOfDogs': 3
     *       }
     *     }
     * @apiSuccess {array} Array of all sitters matching the filter
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *[
     *  {
     *    "id": 2,
     *    "user_id": 2,
     *    "published": true,
     *    "status_id": 1,
     *    "title": "Vitae ut ut ex.",
     *    "slug": "blagomir-bizumic-vitae-ut-ut-ex",
     *    "about": "Consequuntur accusantium omnis doloribus dolores non in et perspiciatis. Dolores soluta esse illum a vitae. Similique voluptatem qui omnis deleniti quod dolor.",
     *    "residence": "apartment",
     *    "kids": null,
     *    "car": null,
     *    "experience": "11",
     *    "cynologist": null,
     *    "veterinary": null,
     *    "about_home": null,
     *    "personal_id": "3561619634620",
     *    "step_finished": 0,
     *    "paypal_email": null,
     *    "paypal_name": null,
     *    "paypal_lastname": null,
     *    "paypal_verification_token": null,
     *    "paypal_verified": null,
     *    "tos": null,
     *    "created_at": "2017-05-08 10:35:38",
     *    "updated_at": "2017-05-08 10:35:38",
     *    "excerpt": "Consequuntur accusantium omnis doloribus dolores non in et perspiciatis. Dolores soluta esse illum a...",
     *    "avatar": "...",
     *    "distance": "4.7",
     *    "servicesFull": [...],
     *    "rating": 2,
     *    "user": {
     *      "id": 2,
     *      "role_id": 2,
     *      "name": "Blagomir Bizumić",
     *      "email": "durdija.vasalic@example.com",
     *      "avatar": "...",
     *      "city": "Kruševac",
     *      "country": "RS",
     *      "gender": "female",
     *      "birthdate": "2001-06-23 00:00:00",
     *      "phone": "(469) 943-1865 x754",
     *      "address": "Nikole Tesle 19 70275 Vranje",
     *      "lat_lng": {
     *        "lat": 44.853946,
     *        "lng": 20.480735
     *      },
     *      "emergency_phone": "+1-430-521-6319",
     *      "emergency_name": "Đenadije",
     *      "verified": 1,
     *      "verification_token": null,
     *      "phone_verified": 1,
     *      "phone_verification_token": null,
     *      "created_at": "2017-05-08 10:35:38",
     *      "updated_at": "2017-05-08 10:35:38",
     *      "rating": 2.5,
     *      "media": [...],
     *      "reviews": [...],
     *      "roles": null,
     *      "pets": [...]
     *      ]
     *    },
     *    "reviews": [ ... ],
     *    "media": [...],
     *    "services": [...],
     *    "sizes": [...]
     *  }
     * ]
     */
    Route::get('/', 'Api\SearchController@index');
    /**
     * @api {get} /sitters/{slug} Show single Sitter
     * @apiName Show single Sitter
     * @apiGroup Sitters
     * @apiVersion 1.0.0
     *
     * @apiParam {string} slug Slug of the Sitter
     *
     * @apiParamExample {string} Request-Example:
     *     {
     *       "slug": "blagomir-bizumic-vitae-ut-ut-ex"
     *     }
     * @apiSuccess {Object} Sitter See Get all sitters for example object
     */
    Route::get('/{slug}', 'Api\SittersController@show');
});

Route::group(['middleware' => ['auth:api']], function (){
    /**
     * @api {get} /me User profile
     * @apiName Show current user
     * @apiGroup Users
     * @apiVersion 1.0.0
     *
     * @apiSuccess {Object} User
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * {
     *  "id": 1,
     *  "role_id": 5,
     *  "name": "Ford Perfect",
     *  "email": "galaxy@unverse.com",
     *  "avatar": "...",
     *  "city": null,
     *  "country": null,
     *  "gender": null,
     *  "birthdate": null,
     *  "phone": null,
     *  "address": "Gospodara Kralja 45",
     *  "lat_lng": {
     *    "lat": 44.815753,
     *    "lng": 20.4563614
     *  },
     *  "emergency_phone": null,
     *  "emergency_name": null,
     *  "verified": 1,
     *  "verification_token": null,
     *  "phone_verified": null,
     *  "phone_verification_token": null,
     *  "created_at": "2017-05-08 10:35:37",
     *  "updated_at": "2017-05-09 14:24:08",
     *  "rating": 0,
     *  "media": [],
     *  "reviews": [],
     *  "roles": null,
     *  "pets": [
     *    {
     *      "id": 91,
     *      "user_id": 1,
     *      "name": "Najda",
     *      "animal_type_id": 2,
     *      "breed_id": 145,
     *      "birthday": "2009-04-13 00:00:00",
     *      "gender": null,
     *      "social": 2,
     *      "sterilised": true,
     *      "vaccinated": null,
     *      "healthy": true,
     *      "healthy_description": "Amet aliquid est cumque quidem voluptatum dolorem quo. Dolor repellendus sunt velit quisquam eveniet non excepturi ea. Dicta qui enim tempore est rem et dolorum. Corrupti soluta corporis labore quod saepe qui.",
     *      "created_at": "2017-05-08 10:35:45",
     *      "updated_at": "2017-05-08 10:35:45",
     *      "avatar": "/img/default-profile.jpg",
     *      "media": []
     *    }
     *  ]
     *}
     */
    Route::get('me', 'UsersController@whoAmI');

    Route::post('messages', 'MessagesController@send');
});



Route::post('update-switches', 'AdController@updateSwitches')->middleware('auth:api');
Route::post('update-app-buttons', 'AdController@updateAppButtons')->middleware('auth:api');
