<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/home', 'HomeController@index');

Route::get('/', function () {
    return view('front.layout');
});
Route::get('/paypal-email-verification/check/{token}', 'AdController@checkPayPalEmail')->name('paypal.email-verification.check');
Route::group(['middleware' => 'auth'], function(){
    Route::get('/profile/edit', 'UsersController@profileForm');
    Route::post('/profile/process-form', 'UsersController@profileUpdate');
    Route::group(['middleware' => 'hasProfile'], function (){
        Route::post('/files/upload', 'FileController@upload');
        Route::get('/pet/create', 'PetController@petForm');
//        Route::post('/pet/create', 'PetController@petSave');

        Route::get('/sitters', 'AdController@index')->name('sitters');
        Route::get('/sitters/register/{step?}', 'AdController@create')->name('sitters.register');
        Route::post('/sitters', 'AdController@store');
        Route::get('/me', 'UsersController@me')->name('my-profile');
        Route::get('/sitters/{slug}', 'AdController@show');
        Route::get('/sitters/{slug}/contact', 'AdController@contact')->name('sitters.contact');
    });

});



//Route::get('/ad/{ad}', 'AdController@frontShow');



//Route::group(['prefix' => 'admin'], function(){
//    Route::resource('pets', 'PetController');
//    Route::resource('services', 'ServiceController');
//    Route::resource('ads', 'AdController');
//});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

