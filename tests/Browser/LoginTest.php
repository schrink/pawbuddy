<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;

class LoginTest extends DuskTestCase {
    use DatabaseMigrations;

    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testLogin()
    {
        $user = factory(User::class)->create([
            'email' => 'taylor@laravel.com'
        ]);

        $this->browse(function ($browser) use ($user)
        {
            $browser->visit('/login')
                ->type('email', $user->email)
                ->type('password', 'secret')
                ->press('Sign In')
                ->assertPathIs('/sitters');
        });
    }

    public function testHome()
    {
        $this->browse(function ($browser)
        {
            $browser->visit('/')
                ->assertSee('Čuvanje Vašeg ljubimca');
        });
    }

    public function testSitters()
    {
        $user = factory(User::class)->create(['verified' => 1]);
        $this->browse(function ($browser) use ($user)
        {
            $browser->loginAs($user)
                ->visit('/sitters')
                ->assertSee('DOK STE NA PUTU');
        });
    }
}
