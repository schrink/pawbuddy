<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;

class UserLoginTest extends TestCase {
    use DatabaseMigrations;

    /**
     * A basic test example.
     *
     * @test
     * @return void
     */
    public function userCannotLoginUntilVerified()
    {
        $user = factory(User::class)->create();
        $user->verified = 0;
        $response = $this->actingAs($user)
            ->get('/sitters');
        $response->assertStatus(500);
    }

    /*
     * @test
     */
    public function testUserCanLoginIfVerified()
    {
        $user = factory(User::class)->create();
        $user->verified = 1;
        $user->save();
        $response = $this->actingAs($user)
            ->get('/sitters');
        $response->assertStatus(200);
    }

    public function testUserCannotContinueIfProfileNotFilled()
    {
        $user = factory(User::class)->create();
        $user->verified = 1;
        $user->address = "";
        $user->save();
        $response = $this->actingAs($user)
            ->get('/sitters');
        $response->assertRedirect('profile/edit');
    }
}
