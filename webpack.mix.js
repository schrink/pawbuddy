const {mix} = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

//mix.js('resources/assets/js/app.js', 'public/js');
mix.js('resources/assets/js/pages/sitters/app.js', 'public/js/sitters');
// .extract(['jquery', 'vue', 'lodash', 'axios', 'sweetalert']);
mix.js('resources/assets/js/pages/profile/app.js', 'public/js/profile');
mix.js('resources/assets/js/pages/sitters-register/app.js', 'public/js/sitters-register');
mix.js('resources/assets/js/pages/me/app.js', 'public/js/me');
mix.js('resources/assets/js/pages/pet/app.js', 'public/js/pet');
// .extract(['jquery', 'vue', 'lodash', 'axios', 'sweetalert']);
mix.js('resources/assets/js/pages/sitters-profile/app.js', 'public/js/sitters-profile');

mix.extract(['jquery', 'vue', 'lodash', 'axios', 'sweetalert', 'vue-form-2', 'element-ui']);

mix.sass('resources/assets/sass/app.scss', 'public/css');
mix.less('resources/assets/less/vendor.less', 'public/css');
//mix.sourceMaps();
if(mix.config.inProduction) mix.version();


mix.webpackConfig({

    devServer: {
      headers: { "Access-Control-Allow-Origin": "*" }
//         proxy: { // proxy URLs to backend development server
//             '*': 'https://localhost:8080'
//         },
//         https: true,
//         // port: 9999
    },
});

mix.sass('resources/assets/sass/front.scss', 'public/css');
mix.options({
  processCssUrls: true
});